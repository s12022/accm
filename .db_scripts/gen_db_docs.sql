BEGIN
    FOR cc_outer IN (
    SELECT table_name
    FROM
        user_tables
    WHERE
        table_name NOT LIKE ('DATABASECHANGELOG%')
        AND
        table_name NOT LIKE ('JHI%')
    ORDER BY table_name
    ) LOOP
        BEGIN
            dbms_output.put_line(cc_outer.table_name);
            dbms_output.put_line('Nazwa kolumny;Typ danych;Nullable;Wartość domyślna;Opis');
            --
            FOR cc_inner IN (
            SELECT
                taco.column_name,
                taco.data_type,
                CASE
                WHEN taco.nullable = 'T'
                    THEN 'Tak'
                ELSE 'Nie'
                END
                    nullable,
                taco.data_default,
                coco.comments
            FROM
                user_tab_cols taco
                LEFT JOIN user_col_comments coco ON (
                taco.table_name = coco.table_name
                AND
                taco.column_name = coco.column_name
                )
            WHERE
                taco.table_name NOT LIKE ('DATABASECHANGELOG%')
                AND
                taco.table_name NOT LIKE ('JHI%')
                AND
                taco.table_name = cc_outer.table_name
            ORDER BY
                taco.table_name,
                taco.column_id
            ) LOOP
                DECLARE
                    l_default_data VARCHAR2(32767) := nvl(cc_inner.data_default, '-brak-');

                BEGIN
                    dbms_output.put_line(cc_inner.column_name || ';' || cc_inner.data_type || ';' || cc_inner.nullable || ';' || l_default_data || ';' || cc_inner.comments);
                END;
            END LOOP cc_inner;
            dbms_output.put_line(chr(13) || chr(10) || CHR(13) || CHR(10));
        END;
    END LOOP cc_outer;
END;