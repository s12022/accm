<<THERAPY_COMPANIES>>
BEGIN
    FOR COMP_THER IN (SELECT
                          comp.ID COID,
                          THER.id THER
                      FROM company comp, THERAPY THER
                      ORDER BY DBMS_RANDOM.RANDOM
                      FETCH FIRST 20 ROWS ONLY)
    LOOP
        BEGIN
            INSERT INTO THERAPY_COMPANIES (COMPANIES_ID, THERAPIES_ID)
            VALUES
                (
                    COMP_THER.COID
                    , COMP_THER.THER
                );
            EXCEPTION
            WHEN dup_val_on_index THEN
            dbms_output.put_line('exception iteration =' || i || ' ' || SQLERRM);
        END;
    END LOOP;
END THERAPY_COMPANIES;
/
