<< MEDICINEBRANCH_COMPANIES >>
BEGIN
    FOR COMP_MEBR IN (SELECT
                          comp.ID COID,
                          mebr.id MEID
                      FROM company comp, MEDICINEBRANCH mebr
                      ORDER BY DBMS_RANDOM.RANDOM
                      FETCH FIRST 20 ROWS ONLY)
    LOOP
        BEGIN
            INSERT INTO MEDICINEBRANCH_COMPANY (COMPANIES_ID, MEDICINEBRANCHES_ID)
            VALUES
                (
                    COMP_MEBR.COID
                    , COMP_MEBR.MEID
                );
            EXCEPTION
            WHEN dup_val_on_index THEN
            dbms_output.put_line('exception iteration =' || i || ' ' || SQLERRM);
        END;
    END LOOP;
END MEDICINEBRANCH_COMPANIES;
/
