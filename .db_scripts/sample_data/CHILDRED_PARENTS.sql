<< CHILDRED_PARENTS >>
BEGIN
    --
    FOR CHILD IN (SELECT ID
                  FROM CHILD)
    LOOP
        DECLARE
            l_chil_paco_no NUMBER := trunc(DBMS_RANDOM.value(0, 3));
        BEGIN
            --
            << consumer >>
            FOR CONSUMER IN (SELECT CONSUMER.ID
                             FROM CONSUMER
                             ORDER BY DBMS_RANDOM.RANDOM
                             FETCH FIRST l_chil_paco_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_CHILDREN (CONSUMERS_ID, CHILDREN_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , CHILD.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of parents ' || SQLERRM);
                END;
            END LOOP consumer;
        END LOOP;
        --commit
        COMMIT;
    END LOOP;
END CHILDRED_PARENTS;
/
