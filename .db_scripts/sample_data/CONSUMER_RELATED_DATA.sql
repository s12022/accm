BEGIN
    --
    FOR CONSUMER IN (SELECT ID
                     FROM CONSUMER)
    LOOP
        DECLARE
            l_cons_prod_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
            l_cons_dise_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
            l_cons_mebr_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
            l_cons_ther_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
            l_cons_daso_no NUMBER := trunc(DBMS_RANDOM.value(1, 4));
        BEGIN
            --
            << products >>
            FOR PRODUCTS IN (SELECT PRODUCT.ID
                             FROM PRODUCT
                             ORDER BY DBMS_RANDOM.RANDOM
                             FETCH FIRST l_cons_prod_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_PRODUCTS (CONSUMERS_ID, PRODUCTS_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , PRODUCTS.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of products ' || SQLERRM);
                END;
            END LOOP products;
            --
            << diseases >>
            FOR DISEASE IN (SELECT DISEASE.ID
                            FROM DISEASE
                            ORDER BY DBMS_RANDOM.RANDOM
                            FETCH FIRST l_cons_dise_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_DISEASES (CONSUMERS_ID, DISEASES_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , DISEASE.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of diseases ' || SQLERRM);
                END;
            END LOOP diseases;
            --
            << branches >>
            FOR BRANCHES IN (SELECT MEDICINEBRANCH.ID
                             FROM MEDICINEBRANCH
                             ORDER BY DBMS_RANDOM.RANDOM
                             FETCH FIRST l_cons_mebr_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_BRANCHES (CONSUMERS_ID, BRANCHES_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , BRANCHES.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of branches ' || SQLERRM);
                END;
            END LOOP branches;
            --
            << datasources >>
            FOR DATASOURCES IN (SELECT DATASOURCE.ID
                                FROM DATASOURCE
                                ORDER BY DBMS_RANDOM.RANDOM
                                FETCH FIRST l_cons_daso_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_DATASOURCES (CONSUMERS_ID, DATASOURCES_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , DATASOURCES.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of datasources ' || SQLERRM);
                END;
            END LOOP datasources;
            --
            << therapies >>
            FOR THERAPIES IN (SELECT THERAPY.ID
                              FROM THERAPY
                              ORDER BY DBMS_RANDOM.RANDOM
                              FETCH FIRST l_cons_ther_no ROWS ONLY)
            LOOP
                BEGIN
                    INSERT INTO CONSUMER_THERAPIES (CONSUMERS_ID, THERAPIES_ID)
                    VALUES
                        (
                            CONSUMER.ID
                            , THERAPIES.ID
                        );
                    EXCEPTION
                    WHEN dup_val_on_index THEN
                    dbms_output.put_line('exception during insertion of therapies ' || SQLERRM);
                END;
            END LOOP therapies;
            --

        END LOOP;
        COMMIT;
    END LOOP;
END;
/
