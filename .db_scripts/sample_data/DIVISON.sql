set define off;
DECLARE
    l_id NUMBER;
BEGIN
    --insert sanofi divisions
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (1, 'Sanofi', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (2, 'Ivostin ', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (3, 'Emolium ', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (4, 'Zentiva ', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (5, ' Sanofi Genzyme', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (6, ' Sanofi Pasteur', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (7, 'Diabetes & Cardiovascular', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (8, 'General Medicines & Emerging Markets', 6);
    INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (9, 'Consumer Healthcare', 6);
    --
    SELECT
        MAX(ID) INTO l_id
    FROM DIVISION;
    --
    FOR company IN (SELECT ID, NAME FROM COMPANY where id != 6)
    LOOP
        BEGIN
            l_id := 1+ l_id;
            INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (l_id, company.name, company.id);
        end;
    END LOOP;
    commit;
END;
/
set define on;
