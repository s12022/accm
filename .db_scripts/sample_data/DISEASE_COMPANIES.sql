<< DISEASE_COMPANIES >>
BEGIN
    FOR COMP_DISE IN (SELECT
                          comp.ID COID,
                          dise.id DIID
                      FROM company comp, disease dise
                      ORDER BY DBMS_RANDOM.RANDOM
                      FETCH FIRST 20 ROWS ONLY)
    LOOP
        BEGIN
            INSERT INTO DISEASE_COMPANIES (COMPANIES_ID, DISEASES_ID)
            VALUES
                (
                    COMP_DISE.COID
                    , COMP_DISE.DIID
                );
            EXCEPTION
            WHEN dup_val_on_index THEN
            dbms_output.put_line('exception iteration =' || i || ' ' || SQLERRM);
        END;
    END LOOP;
END DISEASE_COMPANIES;
/
