BEGIN
    --dbms_output enable
    dbms_output.enable(1000000);

    --DELETE CURRENT DATA
    DELETE MEDICINEBRANCH_COMPANY;
    DELETE DISEASE_COMPANIES;
    DELETE CONSUMER_PRODUCTS;
    DELETE CONSUMER_THERAPIES;
    DELETE CONSUMER_DISEASES;
    DELETE CONSUMER_DATASOURCES;
    DELETE CONSUMER_CHILDREN;
    DELETE CONSUMER_BRANCHES;
    DELETE THERAPY_COMPANIES;
    DELETE CONSUMER;
    DELETE ADDRESS;
    DELETE CHILD;
    DELETE DISEASE;
    DELETE DATASOURCE;
    DELETE ACTIONTYPE;
    DELETE MEDICINEBRANCH;
    DELETE THERAPY;
    DELETE PRODUCT;
    DELETE DIVISION;
    DELETE COMPANY;
    --
    COMMIT;
    --
    --actiontype
    INSERT INTO ACTIONTYPE (id, name) VALUES (1, 'strona internetowa');
    INSERT INTO ACTIONTYPE (id, name) VALUES (2, 'wydarzenie sportowe');
    INSERT INTO ACTIONTYPE (id, name) VALUES (3, 'wydarzenie medyczne');
    INSERT INTO ACTIONTYPE (id, name) VALUES (4, 'impreza masowa');
    INSERT INTO ACTIONTYPE (id, name) VALUES (5, 'ankieta uliczna');
    INSERT INTO ACTIONTYPE (id, name) VALUES (6, 'inny');
    COMMIT;
    --
    --address
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (1, 'Nevada', 'Reno', 'Starling');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (2, 'Texas', 'Dallas', 'Ohio');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (3, 'California', 'Oakland', 'Maryland');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (4, 'California', 'Los Angeles', 'Havey');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (5, 'Virginia', 'Richmond', 'Dahle');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (6, 'Alabama', 'Birmingham', 'Golf Course');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (7, 'Delaware', 'Wilmington', 'Trailsway');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (8, 'Washington', 'Lakewood', 'Mcguire');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (9, 'California', 'San Francisco', 'Mariners Cove');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (10, 'North Carolina', 'High Point', 'Dakota');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (11, 'Minnesota', 'Minneapolis', 'Northwestern');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (12, 'Minnesota', 'Minneapolis', 'Mallard');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (13, 'Georgia', 'Alpharetta', 'Elmside');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (14, 'New Jersey', 'Trenton', 'High Crossing');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (15, 'Massachusetts', 'Boston', 'Arapahoe');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (16, 'Florida', 'Fort Myers', 'Miller');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (17, 'New York', 'Bronx', 'Rigney');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (18, 'Arizona', 'Tempe', 'La Follette');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (19, 'Pennsylvania', 'Pittsburgh', 'Mendota');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (20, 'Florida', 'Panama City', 'Gateway');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (21, 'Florida', 'Fort Lauderdale', 'Bunker Hill');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (22, 'Texas', 'Bryan', 'Golf Course');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (23, 'Mississippi', 'Jackson', '8th');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (24, 'Florida', 'Miami', 'Anhalt');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (25, 'Georgia', 'Savannah', 'Reindahl');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (26, 'Arizona', 'Chandler', 'Truax');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (27, 'Tennessee', 'Memphis', 'Maple');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (28, 'Georgia', 'Lawrenceville', 'Myrtle');
    INSERT INTO ADDRESS (ID, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (29, 'Texas', 'Tyler', 'Almo');
    INSERT INTO ADDRESS (id, PROVINCE_NAME, CITY_NAME, STREET_NAME) VALUES (30, 'Texas', 'San Antonio', 'Thierer');
    --
    COMMIT;
    --
    --children
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (1, 'Chere', to_timestamp('2005-06-27 05:15:05', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (2, 'Jenny', to_timestamp('2014-02-20 13:37:43', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (3, 'Peg', to_timestamp('2003-07-31 07:24:26', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (4, 'Baudoin', to_timestamp('2004-08-11 17:48:52', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (5, 'Alika', to_timestamp('2013-01-08 00:36:23', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (6, 'Gaylor', to_timestamp('2011-12-08 18:40:06', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (7, 'Ricardo', to_timestamp('2010-01-04 20:34:05', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (8, 'Marie-jeanne', to_timestamp('2009-05-05 12:48:52', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (9, 'Elianore', to_timestamp('2004-05-26 06:26:48', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (10, 'Rochelle', to_timestamp('2015-04-27 15:15:51', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (11, 'Millard', to_timestamp('2012-03-16 14:08:58', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (12, 'Orly', to_timestamp('2005-04-22 10:11:32', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (13, 'Elisabeth', to_timestamp('2013-12-07 03:19:03', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (14, 'Ogdan', to_timestamp('2003-07-01 18:04:32', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (15, 'Rianon', to_timestamp('2006-12-26 19:51:21', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (16, 'Geordie', to_timestamp('2011-08-11 21:26:46', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (17, 'Bathsheba', to_timestamp('2009-10-24 15:44:21', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (18, 'Kira', to_timestamp('2006-02-19 21:22:55', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (19, 'Querida', to_timestamp('2005-01-01 03:08:30', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (20, 'Alexa', to_timestamp('2008-01-14 18:39:49', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (21, 'Nicolette', to_timestamp('2015-04-18 16:27:24', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (22, 'Vidovik', to_timestamp('2006-12-31 04:43:42', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (23, 'Nissa', to_timestamp('2012-11-23 11:29:36', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (24, 'Gunar', to_timestamp('2001-11-09 13:29:54', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (25, 'Bekki', to_timestamp('2006-07-25 21:39:56', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (26, 'Sibelle', to_timestamp('2009-01-15 04:39:34', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (27, 'Vivi', to_timestamp('2016-12-16 06:17:59', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (28, 'Maurise', to_timestamp('2007-06-18 21:00:44', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (29, 'Renato', to_timestamp('2014-05-19 12:40:38', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (30, 'Arleen', to_timestamp('2017-02-21 06:54:20', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (31, 'Daniela', to_timestamp('2016-06-22 00:48:17', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (32, 'Scarlett', to_timestamp('2007-02-17 12:59:04', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (33, 'Orion', to_timestamp('2006-12-31 21:37:00', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (34, 'Kippie', to_timestamp('2008-03-09 03:00:57', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (35, 'Juliet', to_timestamp('2007-08-30 10:10:54', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (36, 'Brandise', to_timestamp('2007-01-28 14:41:55', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (37, 'Glyn', to_timestamp('2012-09-10 09:46:55', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (38, 'Raquel', to_timestamp('2010-05-02 10:57:04', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (39, 'Cynthy', to_timestamp('2012-06-27 13:39:41', 'YYYY-MM-DD HH24:MI:SS'));
    INSERT INTO CHILD (id, NAME, BIRTHDATE)
    VALUES (40, 'Cirstoforo', to_timestamp('2005-06-21 09:37:04', 'YYYY-MM-DD HH24:MI:SS'));
    --
    COMMIT;
    --
    --companies
    INSERT INTO COMPANY (id, name) VALUES (1, 'Polfa');
    INSERT INTO COMPANY (id, name) VALUES (2, 'Jelfa');
    INSERT INTO COMPANY (id, name) VALUES (3, 'Cefarm Szczecin');
    INSERT INTO COMPANY (id, name) VALUES (4, 'Bioton');
    INSERT INTO COMPANY (id, name) VALUES (5, 'Gedeon Richter plc');
    INSERT INTO COMPANY (id, name) VALUES (6, 'Sanofi Aventis');
    --
    COMMIT;
    --
    --
    --divisions
    << divisions >>
    DECLARE
        l_id NUMBER;
    BEGIN
        --insert sanofi divisions
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (1, 'Sanofi', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (2, 'Ivostin ', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (3, 'Emolium ', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (4, 'Zentiva ', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (5, ' Sanofi Genzyme', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (6, ' Sanofi Pasteur', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (7, 'Diabetes AND Cardiovascular', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (8, 'General Medicines AND Emerging Markets', 6);
        INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (9, 'Consumer Healthcare', 6);
        --
        SELECT
            MAX(ID) INTO l_id
        FROM DIVISION;
        --
        FOR company IN (SELECT ID, NAME FROM COMPANY where id != 6)
        LOOP
            BEGIN
                l_id := 1+ l_id;
                INSERT INTO DIVISION (ID, NAME, COMPANY_ID) VALUES (l_id, company.name, company.id);
            end;
        END LOOP;
        commit;
    END divisions;
    --
    --datasources
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (1, 'Maraton Podlaski', to_timestamp('2016-02-03 20:32:00',  'YYYY-MM-DD HH24:MI:SS' ), 1, 'James Bougen', 2, 1);
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (2, 'przystanek woodstok', to_timestamp('2017-08-16 17:18:10',  'YYYY-MM-DD HH24:MI:SS' ), NULL, 'Kessia McClifferty', 4, 1);
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (3, 'zentiva nowy banner', to_timestamp('2017-04-06 19:01:40',  'YYYY-MM-DD HH24:MI:SS' ), 1, 'Jaine Hutchinges', 1, 1);
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (4, 'Maraton Podlaski', to_timestamp('2017-02-03 20:32:00',  'YYYY-MM-DD HH24:MI:SS' ), 2, 'James Bougen', 2, 1);
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (5, 'Biegnij Warszawo', to_timestamp('2017-01-11 08:20:00',  'YYYY-MM-DD HH24:MI:SS' ), 1, 'Willis Casperri', 2, 3);
    insert into DATASOURCE (id, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    values (6, 'Zlote tarasy targi zdrowia', to_timestamp('2016-11-07 09:42:00',  'YYYY-MM-DD HH24:MI:SS' ), 1, 'Lettie Alfonso', 6, 1);
    insert into DATASOURCE (id, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    values (7, 'potenti cras in', to_timestamp('2017-02-06 15:00:00',  'YYYY-MM-DD HH24:MI:SS' ), 4, 'Clare Odda', (SELECT id FROM ACTIONtYPE ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only), (SELECT id FROM DIVISION ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into DATASOURCE (id, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    values (8, 'neque vestibulum', to_timestamp('2017-07-23 04:16:50',  'YYYY-MM-DD HH24:MI:SS' ), 6, 'Melinda Jurzyk', (SELECT id FROM ACTIONtYPE ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only), (SELECT id FROM DIVISION ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into DATASOURCE (id, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    values (9, 'integer aliquet massa', to_timestamp('2017-06-01 13:50:40',  'YYYY-MM-DD HH24:MI:SS' ), 4, 'Merell Camelli', (SELECT id FROM ACTIONtYPE ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only), (SELECT id FROM DIVISION ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (10, 'lobortis', to_timestamp('2017-01-24 00:16:20',  'YYYY-MM-DD HH24:MI:SS' ), 2, 'Maryellen Branton', (SELECT id FROM ACTIONtYPE ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only), (SELECT id FROM DIVISION ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (11, 'nibh quisque id', to_timestamp('2016-12-04 05:06:40',  'YYYY-MM-DD HH24:MI:SS' ), 2, 'Stanford Fiddy', (SELECT id FROM ACTIONtYPE ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only), (SELECT id FROM DIVISION ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (12, 'Zlote tarasy targi zdrowia', to_timestamp('2017-02-07 09:42:00',  'YYYY-MM-DD HH24:MI:SS' ), 2, 'Lettie Alfonso', 6, 1);
    INSERT INTO DATASOURCE (ID, title, jhi_date, turn, PERSON_NAME, ACTIONTYPE_ID, DIVISION_ID)
    VALUES (13, 'Maraton Podlaski', to_timestamp('2017-02-03 20:32:00',  'YYYY-MM-DD HH24:MI:SS' ),3, 'James Bougen', 2, 1);
    --
    COMMIT;
    --
    --diseases
    INSERT INTO DISEASE (id, name) VALUES (1, 'Relaxation of scar');
    INSERT INTO DISEASE (id, name) VALUES (2, 'Incision uterine septum');
    INSERT INTO DISEASE (id, name) VALUES (3, 'Inj therap subst periton');
    INSERT INTO DISEASE (id, name) VALUES (4, 'Closed prostatic biopsy');
    INSERT INTO DISEASE (id, name) VALUES (5, 'Digital exam of uterus');
    INSERT INTO DISEASE (id, name) VALUES (6, 'Hypothermia');
    INSERT INTO DISEASE (id, name) VALUES (7, 'Insert bone stimul NEC');
    INSERT INTO DISEASE (id, name) VALUES (8, 'Brain repair');
    INSERT INTO DISEASE (id, name) VALUES (9, 'Other bone dx proc NOS');
    INSERT INTO DISEASE (id, name) VALUES (10, 'Upper limb amputat NOS');
    --
    COMMIT;
    --
    --medicinebranches
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (1, 'Cardiology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (2, 'Critical care medicine');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (3, 'Dentistry');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (4, 'Dermatology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (5, 'Emergency medicine');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (6, 'Endocrinology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (7, 'Epidemiology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (8, 'First aid');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (9, 'Gastroenterology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (10, 'General practice');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (11, 'Geriatrics');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (12, 'Gynaecology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (13, 'Hematology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (14, 'Hepatology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (15, 'Infectious disease');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (16, 'Internal medicine');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (17, 'Neurology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (18, 'Nephrology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (19, 'Obstetrics');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (20, 'Oncology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (21, 'Ophthalmology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (22, 'Orthopaedics');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (23, 'Otorhinolaryngology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (24, 'Pathology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (25, 'Pediatrics');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (26, 'Preventive medicine');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (27, 'Psychiatry');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (28, 'Pulmonology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (29, 'Radiology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (30, 'Sports medicine');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (31, 'Rheumatology');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (32, 'Surgery');
    INSERT INTO MEDICINEBRANCH (id, name) VALUES (33, 'Urology');
    --
    COMMIT;
    --
    --therapies
    INSERT INTO THERAPY (id, name) VALUES (1, 'Trnspl status-pancreas');
    INSERT INTO THERAPY (id, name) VALUES (2, 'Arthropod hem fever NEC');
    INSERT INTO THERAPY (id, name) VALUES (3, 'Uterine inert NEC-deliv');
    INSERT INTO THERAPY (id, name) VALUES (4, 'Prepatellar bursitis');
    INSERT INTO THERAPY (id, name) VALUES (5, 'Adjust react-withdrawal');
    INSERT INTO THERAPY (id, name) VALUES (6, 'Bact arthritis-hand');
    INSERT INTO THERAPY (id, name) VALUES (7, 'Carbuncle NOS');
    INSERT INTO THERAPY (id, name) VALUES (8, 'Pupillary margin degen');
    INSERT INTO THERAPY (id, name) VALUES (9, '3 deg burn back of hand');
    INSERT INTO THERAPY (id, name) VALUES (10, 'Adjustment reaction NOS');
    --
    COMMIT;
    --
    --products
    insert into PRODUCT (id, name, company_id)
    values (1, 'CareOne Mucus ER', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (2, 'NALTREXONE HYDROCHLORIDE', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (3, 'good sense all day pain relief', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (4, 'HOT MOVATE', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (5, 'TEV-TROPIN', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (6, 'Lisinopril', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (7, 'Sinus Rescue', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (8, 'Medskin Cleansing', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (9, 'napoleon PERDIS CHINA DOLL', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (10, 'Aruba Aloe Very Water Resistant Sunscreen', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (11, 'SOTALOL HYDROCHLORIDE', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (12, 'Oxybutynin Chloride', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (13, 'IMDUR', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (14, 'Hepatatox', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (15, 'Ambien', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (16, 'FENTANYL TRANSDERMAL SYSTEM', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (17, 'Health Smart Antibacterial original', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (18, 'Childrens Nighttime Cold and Cough', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (19, 'Zipsor', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (20, 'PATANASE', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (21, 'Lodrane D', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (22, 'RITE AID RENEWAL', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (23, 'Liothyronine sodium', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (24, 'Mushroom', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (25, 'Carbon Dioxide-Air Mixture', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (26, 'PedvaxHIB', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (27, 'Glycopyrrolate', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (28, 'Levetiracetam', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (29, 'OXYTROL', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (30, 'Aspergillus flavus', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (31, 'Streptomyces', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (32, 'Apiol', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (33, 'Rehydrate', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (34, 'Thyroveev', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (35, 'Care One Medicated Lip Balm in a Pot', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (36, 'NATURA-LAX', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (37, 'Lovastatin', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (38, 'Paroxetine', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (39, 'Soft Care Antiseptic Skin Cleanser', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into PRODUCT (id, name, company_id)
    values (40, 'Mucinex', (SELECT id FROM company  ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    --
    COMMIT;
    --
    --consumers
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (1, 'Talia', 'Ladbrooke', 'tladbrooke0@drupal.org', null, 1, 0, '947-919-6551', 'SPECIALIST', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (2, 'Kora', 'Portchmouth', 'kportchmouth1@reuters.com', 'OTHER', 1, 0, '300-324-6711', 'SPECIALIST', 'semper sapien a', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (3, 'Michelina', 'Munnis', 'mmunnis2@lulu.com', 'FEMALE', 0, 0, '804-408-5881', 'CONSUMER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (4, 'Karalynn', 'Delagnes', 'kdelagnes3@indiatimes.com', 'OTHER', 1, 1, '339-442-9252', 'CONSUMER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (5, 'Hinda', 'Robinet', 'hrobinet4@dagondesign.com', 'MALE', 0, 1, '583-393-7370', 'CONSUMER', 'tellus semper interdum mauris ullamcorper purus sit', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (6, 'Zara', 'Coyne', 'zcoyne5@merriam-webster.com', 'FEMALE', 1, 0, '513-886-8375', 'CONSUMER', 'morbi sem mauris laoreet ut rhoncus', null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (7, 'North', 'Trussler', 'ntrussler6@surveymonkey.com', 'OTHER', 1, 1, '671-403-2063', 'SPECIALIST', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (8, 'Clyde', 'Dallas', 'cdallas7@businesswire.com', 'MALE', 1, 1, '282-339-4939', 'SPECIALIST', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (9, 'Forbes', 'Hasel', 'fhasel8@g.co', 'OTHER', 1, 1, '260-653-4244', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (10, 'Melanie', 'Cristofolini', 'mcristofolini9@xing.com', 'OTHER', 1, 0, '275-851-4531', 'SPECIALIST', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (11, 'Dallas', 'Farndell', 'dfarndella@paypal.com', 'OTHER', 1, 1, '490-714-5816', 'CONSUMER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (12, 'Carlos', 'Bahl', 'cbahlb@cargocollective.com', 'OTHER', 1, 0, '650-248-5360', 'CONSUMER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (13, 'Waldon', 'Bennedick', 'wbennedickc@live.com', 'FEMALE', 1, 0, '133-317-7449', 'SPECIALIST', 'consequat dui nec', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (14, 'Amelia', 'Johnys', 'ajohnysd@go.com', 'OTHER', 1, 0, '969-300-3386', 'CONSUMER', 'arcu sed augue aliquam erat volutpat in congue etiam', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (15, 'Minna', 'Fulger', 'mfulgere@wufoo.com', 'OTHER', 0, 1, '492-310-4547', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (16, 'Al', 'Brobyn', 'abrobynf@nba.com', 'FEMALE', 0, 0, '350-184-6551', 'CONSUMER', 'eu nibh quisque id justo sit', null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (17, 'Svend', 'Weich', 'sweichg@ow.ly', null, 1, 0, '462-545-3045', 'SPECIALIST', 'vehicula condimentum curabitur in', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (18, 'Colline', 'Mardling', 'cmardlingh@cnet.com', 'OTHER', 0, 1, '904-971-8712', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (19, 'Florrie', 'Henrych', 'fhenrychi@home.pl', 'OTHER', 0, 1, '414-690-0443', 'CONSUMER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (20, 'Bessy', 'Peascod', 'bpeascodj@nydailynews.com', null, 1, 1, '321-777-8096', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (21, 'Glynda', 'Twinn', 'gtwinnk@alexa.com', 'OTHER', 0, 1, '683-760-3196', 'CONSUMER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (22, 'Winni', 'Lickess', 'wlickessl@quantcast.com', null, 0, 0, '826-608-6819', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (23, 'Geri', 'Antley', 'gantleym@ovh.net', 'MALE', 0, 1, '400-506-5543', 'SPECIALIST', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (24, 'Baxie', 'Coarser', 'bcoarsern@tripadvisor.com', 'FEMALE', 0, 1, '451-838-6570', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (25, 'Malynda', 'Fleeming', 'mfleemingo@clickbank.net', 'FEMALE', 1, 1, '916-879-5883', 'CONSUMER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (26, 'Gris', 'Friar', 'gfriarp@mtv.com', 'FEMALE', 0, 1, '184-834-8050', 'SPECIALIST', 'faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (27, 'Britt', 'Poulney', 'bpoulneyq@themeforest.net', 'OTHER', 0, 0, '835-808-7432', 'CONSUMER', 'accumsan odio curabitur', null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (28, 'Binni', 'Burdett', 'bburdettr@un.org', 'OTHER', 1, 1, '646-813-1090', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (29, 'Roshelle', 'Bowle', 'rbowles@diigo.com', null, 1, 1, '923-691-3498', 'SPECIALIST', 'tempus sit amet sem', null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (30, 'Zelig', 'Beatens', 'zbeatenst@gov.uk', 'OTHER', 0, 0, '784-769-7173', 'CONSUMER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (31, 'Luella', 'Piwell', 'lpiwellu@who.int', 'FEMALE', 0, 0, '385-933-2212', 'OTHER', 'praesent blandit nam nulla integer pede', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (32, 'Patrizia', 'Jeffs', 'pjeffsv@usa.gov', 'OTHER', 1, 1, '192-905-3171', 'OTHER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (33, 'Bax', 'Milmith', 'bmilmithw@intel.com', 'OTHER', 1, 1, '895-290-1333', 'OTHER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (34, 'Merrel', 'Addington', 'maddingtonx@forbes.com', 'MALE', 1, 1, '318-116-5295', 'OTHER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (35, 'Gustie', 'Furness', 'gfurnessy@eepurl.com', null, 0, 0, '285-963-1611', 'CONSUMER', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (36, 'Katee', 'Allonby', 'kallonbyz@webmd.com', 'MALE', 0, 0, '228-592-0072', 'OTHER', null, (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (37, 'Heinrik', 'Provest', 'hprovest10@odnoklassniki.ru', 'MALE', 1, 1, '160-965-4049', 'SPECIALIST', 'a feugiat et eros vestibulum ac est lacinia', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (38, 'Clemmie', 'Carden', 'ccarden11@t.co', 'MALE', 0, 1, '749-296-6082', 'SPECIALIST', null, null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (39, 'Alfy', 'Cadney', 'acadney12@creativecommons.org', null, 1, 1, '141-641-4296', 'CONSUMER', 'bibendum felis sed interdum venenatis turpis enim', null);
    insert into CONSUMER (id, first_name, last_name, email, gender, accepted, active, PHONE_NUMBER, USER_TYPE, USER_COMMENT, ADDRESS_ID)
    values (40, 'Shaylyn', 'Laweles', 'slaweles13@reuters.com', 'OTHER', 1, 0, '931-670-5744', 'CONSUMER', 'leo odio condimentum id luctus nec molestie sed', (sELECT id FROM address ORDER BY DBMS_RANDOM.RANDOM fetch first 1 rows only));
    --
    --
    --
    << CHILDRED_PARENTS >>
    BEGIN
        --
        FOR CHILD IN (SELECT ID
                      FROM CHILD)
        LOOP
            DECLARE
                l_chil_paco_no NUMBER := trunc(DBMS_RANDOM.value(0, 3));
            BEGIN
                --
                << consumer >>
                FOR CONSUMER IN (SELECT CONSUMER.ID
                                 FROM CONSUMER
                                 ORDER BY DBMS_RANDOM.RANDOM
                                 FETCH FIRST l_chil_paco_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_CHILDREN (CONSUMERS_ID, CHILDREN_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , CHILD.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of parents ' || SQLERRM);
                    END;
                END LOOP consumer;
            END LOOP;

        END LOOP;
    END CHILDRED_PARENTS;
    --
    --consumer related data
    << CONSUMER_RELATED_DATA >>
    BEGIN
        --
        FOR CONSUMER IN (SELECT ID
                         FROM CONSUMER)
        LOOP
            DECLARE
                l_cons_prod_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
                l_cons_dise_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
                l_cons_mebr_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
                l_cons_ther_no NUMBER := trunc(DBMS_RANDOM.value(1, 11));
                l_cons_daso_no NUMBER := trunc(DBMS_RANDOM.value(1, 4));
            BEGIN
                --
                << products >>
                FOR PRODUCTS IN (SELECT PRODUCT.ID
                                 FROM PRODUCT
                                 ORDER BY DBMS_RANDOM.RANDOM
                                 FETCH FIRST l_cons_prod_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_PRODUCTS (CONSUMERS_ID, PRODUCTS_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , PRODUCTS.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of products ' || SQLERRM);
                    END;
                END LOOP products;

                --
                << diseases >>
                FOR DISEASE IN (SELECT DISEASE.ID
                                FROM DISEASE
                                ORDER BY DBMS_RANDOM.RANDOM
                                FETCH FIRST l_cons_dise_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_DISEASES (CONSUMERS_ID, DISEASES_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , DISEASE.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of diseases ' || SQLERRM);
                    END;
                END LOOP diseases;

                --
                << branches >>
                FOR BRANCHES IN (SELECT MEDICINEBRANCH.ID
                                 FROM MEDICINEBRANCH
                                 ORDER BY DBMS_RANDOM.RANDOM
                                 FETCH FIRST l_cons_mebr_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_BRANCHES (CONSUMERS_ID, BRANCHES_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , BRANCHES.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of branches ' || SQLERRM);
                    END;
                END LOOP branches;

                --
                << datasources >>
                FOR DATASOURCES IN (SELECT DATASOURCE.ID
                                    FROM DATASOURCE
                                    ORDER BY DBMS_RANDOM.RANDOM
                                    FETCH FIRST l_cons_daso_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_DATASOURCES (CONSUMERS_ID, DATASOURCES_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , DATASOURCES.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of datasources ' || SQLERRM);
                    END;
                END LOOP datasources;
                --
                << therapies >>
                FOR THERAPIES IN (SELECT THERAPY.ID
                                  FROM THERAPY
                                  ORDER BY DBMS_RANDOM.RANDOM
                                  FETCH FIRST l_cons_ther_no ROWS ONLY)
                LOOP
                    BEGIN
                        INSERT INTO CONSUMER_THERAPIES (CONSUMERS_ID, THERAPIES_ID)
                        VALUES
                            (
                                CONSUMER.ID
                                , THERAPIES.ID
                            );
                        EXCEPTION
                        WHEN dup_val_on_index THEN
                        dbms_output.put_line('exception during insertion of therapies ' || SQLERRM);
                    END;
                END LOOP therapies;
                --
            END LOOP;
        END LOOP;
        COMMIT;
    END CONSUMER_RELATED_DATA;
    --
    --
    << MEDICINEBRANCH_COMPANIES >>
    BEGIN
        FOR COMP_MEBR IN (SELECT
                              comp.ID COID,
                              mebr.id MEID
                          FROM company comp, MEDICINEBRANCH mebr
                          ORDER BY DBMS_RANDOM.RANDOM
                          FETCH FIRST 20 ROWS ONLY)
        LOOP
            BEGIN
                INSERT INTO MEDICINEBRANCH_COMPANY (COMPANIES_ID, MEDICINEBRANCHES_ID)
                VALUES
                    (
                        COMP_MEBR.COID
                        , COMP_MEBR.MEID
                    );
                EXCEPTION
                WHEN dup_val_on_index THEN
                dbms_output.put_line(SQLERRM);
            END;
        END LOOP;
        COMMIT;
    END MEDICINEBRANCH_COMPANIES;
    --
    --
    <<THERAPY_COMPANIES>>
    BEGIN
        FOR COMP_THER IN (SELECT
                              comp.ID COID,
                              THER.id THER
                          FROM company comp, THERAPY THER
                          ORDER BY DBMS_RANDOM.RANDOM
                          FETCH FIRST 20 ROWS ONLY)
        LOOP
            BEGIN
                INSERT INTO THERAPY_COMPANIES (COMPANIES_ID, THERAPIES_ID)
                VALUES
                    (
                        COMP_THER.COID
                        , COMP_THER.THER
                    );
                EXCEPTION
                WHEN dup_val_on_index THEN
                dbms_output.put_line(SQLERRM);
            END;
        END LOOP;
        COMMIT;
    END THERAPY_COMPANIES;
    --
    --
    << DISEASE_COMPANIES >>
    BEGIN
        FOR COMP_DISE IN (SELECT
                              comp.ID COID,
                              dise.id DIID
                          FROM company comp, disease dise
                          ORDER BY DBMS_RANDOM.RANDOM
                          FETCH FIRST 20 ROWS ONLY)
        LOOP
            BEGIN
                INSERT INTO DISEASE_COMPANIES (COMPANIES_ID, DISEASES_ID)
                VALUES
                    (
                        COMP_DISE.COID
                        , COMP_DISE.DIID
                    );
                EXCEPTION
                WHEN dup_val_on_index THEN
                dbms_output.put_line('exception '  || SQLERRM);
            END;
        END LOOP;
        COMMIT;
    END DISEASE_COMPANIES;
    COMMIT;
    dbms_output.put_line('OK');
END;
/
