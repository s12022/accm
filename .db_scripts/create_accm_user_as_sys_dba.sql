SET time on
SET ECHO ON;
DEFINE accm_db          = "orcl12c"
SPOOL OFF
SPOOL create_user.log APPEND
PROMPT "To spowoduje usunięcie schematu ACCM ze wszystkimi danymi, a następnie utworzeie schematu ACCM"
ACCEPT do_next PROMPT "Czy kontynouwać? Tak - dowolny klawisz, Nie - CTRL+C : "
SET SERVEROUTPUT ON
ACCEPT sys_password CHAR PROMPT 'Podaj hasło użytkownika sys: ' HIDE
CONNECT sys/&sys_password@&accm_db as SYSDBA
----
DROP USER ACCM CASCADE;

CREATE USER ACCM IDENTIFIED BY ACCM
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS;

GRANT CONNECT,RESOURCE,CREATE VIEW TO ACCM;
--
--
DISCONNECT;

SPOOL OFF
EXIT SUCCESS
