SET time on
SET ECHO ON;
DEFINE ACCM_DEV_db          = "orcl12c"
SPOOL OFF
SPOOL create_user.log APPEND
PROMPT "To spowoduje usunięcie uzytkowonika ACCM_DEV ze wszystkimi danymi, a następnie utworzeie schematu ACCM_DEV"
ACCEPT do_next PROMPT "Czy kontynouwać? Tak - dowolny klawisz, Nie - CTRL+C : "
SET SERVEROUTPUT ON
ACCEPT sys_password CHAR PROMPT 'Podaj hasło użytkownika sys: ' HIDE
CONNECT sys/&sys_password@&ACCM_DEV_db as SYSDBA
----
DROP USER ACCM_DEV CASCADE;

CREATE USER ACCM_DEV IDENTIFIED BY ACCM_DEV
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS;

GRANT CONNECT,RESOURCE,CREATE VIEW TO ACCM_DEV;
--
--
DISCONNECT;

SPOOL OFF
EXIT SUCCESS
