SET time on
SET ECHO ON;
DEFINE accm_db          = "orcl12c"
DEFINE accm_password    = "ACCM"
SPOOL OFF
SPOOL insertingTestData.log APPEND
PROMPT "INSERT TEST DATA"
PROMPT "To spowoduje usuniecie danych w bazie accm"
ACCEPT do_next PROMPT "Czy kontynouwać? Tak - dowolny klawisz, Nie - CTRL+C : "
SET SERVEROUTPUT ON
CONNECT ACCM/&accm_password@&accm_db
    @@GENERATE_AND_INSERT_TEST_DATA.sql
DISCONNECT;

SPOOL OFF
EXIT SUCCESS
