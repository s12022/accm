SET time on
SET ECHO ON;
DEFINE ACCM_DEV_db          = "orcl12c"
DEFINE ACCM_DEV_password    = "ACCM_DEV"
SPOOL OFF
SPOOL insertingTestDataDev.log APPEND
PROMPT "INSERT TEST DATA"
PROMPT "To spowoduje usuniecie danych w bazie ACCM_DEV"
ACCEPT do_next PROMPT "Czy kontynouwać? Tak - dowolny klawisz, Nie - CTRL+C : "
SET SERVEROUTPUT ON
CONNECT ACCM_DEV/&ACCM_DEV_password@&ACCM_DEV_db
    @@GENERATE_AND_INSERT_TEST_DATA.sql
DISCONNECT;

SPOOL OFF
EXIT SUCCESS
