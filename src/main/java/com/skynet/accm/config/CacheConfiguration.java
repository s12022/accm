package com.skynet.accm.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.skynet.accm.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".therapies", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".diseases", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".branches", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".children", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Consumer.class.getName() + ".datasources", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Child.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Child.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Address.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Product.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Product.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Disease.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Disease.class.getName() + ".companies", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Disease.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Therapy.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Therapy.class.getName() + ".companies", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Therapy.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Medicinebranch.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Medicinebranch.class.getName() + ".companies", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Medicinebranch.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Datasource.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Datasource.class.getName() + ".consumers", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Actiontype.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Division.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Division.class.getName() + ".datasources", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName(), jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName() + ".divisions", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName() + ".therapies", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName() + ".diseases", jcacheConfiguration);
            cm.createCache(com.skynet.accm.domain.Company.class.getName() + ".branches", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
