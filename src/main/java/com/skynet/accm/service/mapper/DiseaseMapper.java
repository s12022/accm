package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.DiseaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Disease and its DTO DiseaseDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class, })
public interface DiseaseMapper extends EntityMapper <DiseaseDTO, Disease> {
    
    @Mapping(target = "consumers", ignore = true)
    Disease toEntity(DiseaseDTO diseaseDTO); 
    default Disease fromId(Long id) {
        if (id == null) {
            return null;
        }
        Disease disease = new Disease();
        disease.setId(id);
        return disease;
    }
}
