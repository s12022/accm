package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.ActiontypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Actiontype and its DTO ActiontypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ActiontypeMapper extends EntityMapper <ActiontypeDTO, Actiontype> {
    
    
    default Actiontype fromId(Long id) {
        if (id == null) {
            return null;
        }
        Actiontype actiontype = new Actiontype();
        actiontype.setId(id);
        return actiontype;
    }
}
