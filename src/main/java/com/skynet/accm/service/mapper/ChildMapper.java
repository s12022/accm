package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.ChildDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Child and its DTO ChildDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChildMapper extends EntityMapper <ChildDTO, Child> {
    
    @Mapping(target = "consumers", ignore = true)
    Child toEntity(ChildDTO childDTO); 
    default Child fromId(Long id) {
        if (id == null) {
            return null;
        }
        Child child = new Child();
        child.setId(id);
        return child;
    }
}
