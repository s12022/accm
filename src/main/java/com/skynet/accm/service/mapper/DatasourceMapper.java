package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.DatasourceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Datasource and its DTO DatasourceDTO.
 */
@Mapper(componentModel = "spring", uses = {ActiontypeMapper.class, DivisionMapper.class, })
public interface DatasourceMapper extends EntityMapper <DatasourceDTO, Datasource> {

    @Mapping(source = "actiontype.id", target = "actiontypeId")
    @Mapping(source = "actiontype.name", target = "actiontypeName")

    @Mapping(source = "division.id", target = "divisionId")
    @Mapping(source = "division.name", target = "divisionName")
    DatasourceDTO toDto(Datasource datasource); 

    @Mapping(source = "actiontypeId", target = "actiontype")

    @Mapping(source = "divisionId", target = "division")
    @Mapping(target = "consumers", ignore = true)
    Datasource toEntity(DatasourceDTO datasourceDTO); 
    default Datasource fromId(Long id) {
        if (id == null) {
            return null;
        }
        Datasource datasource = new Datasource();
        datasource.setId(id);
        return datasource;
    }
}
