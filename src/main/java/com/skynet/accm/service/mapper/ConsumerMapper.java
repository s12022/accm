package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.ConsumerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Consumer and its DTO ConsumerDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class, ProductMapper.class, TherapyMapper.class, DiseaseMapper.class, MedicinebranchMapper.class, ChildMapper.class, DatasourceMapper.class, })
public interface ConsumerMapper extends EntityMapper <ConsumerDTO, Consumer> {

    @Mapping(source = "address.id", target = "addressId")
    ConsumerDTO toDto(Consumer consumer); 

    @Mapping(source = "addressId", target = "address")
    Consumer toEntity(ConsumerDTO consumerDTO); 
    default Consumer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Consumer consumer = new Consumer();
        consumer.setId(id);
        return consumer;
    }
}
