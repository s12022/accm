package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.DivisionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Division and its DTO DivisionDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class, })
public interface DivisionMapper extends EntityMapper <DivisionDTO, Division> {

    @Mapping(source = "company.id", target = "companyId")
    @Mapping(source = "company.name", target = "companyName")
    DivisionDTO toDto(Division division); 

    @Mapping(source = "companyId", target = "company")
    @Mapping(target = "datasources", ignore = true)
    Division toEntity(DivisionDTO divisionDTO); 
    default Division fromId(Long id) {
        if (id == null) {
            return null;
        }
        Division division = new Division();
        division.setId(id);
        return division;
    }
}
