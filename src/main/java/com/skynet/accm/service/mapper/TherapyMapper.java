package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.TherapyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Therapy and its DTO TherapyDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class, })
public interface TherapyMapper extends EntityMapper <TherapyDTO, Therapy> {
    
    @Mapping(target = "consumers", ignore = true)
    Therapy toEntity(TherapyDTO therapyDTO); 
    default Therapy fromId(Long id) {
        if (id == null) {
            return null;
        }
        Therapy therapy = new Therapy();
        therapy.setId(id);
        return therapy;
    }
}
