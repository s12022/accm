package com.skynet.accm.service.mapper;

import com.skynet.accm.domain.*;
import com.skynet.accm.service.dto.MedicinebranchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Medicinebranch and its DTO MedicinebranchDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class, })
public interface MedicinebranchMapper extends EntityMapper <MedicinebranchDTO, Medicinebranch> {
    
    @Mapping(target = "consumers", ignore = true)
    Medicinebranch toEntity(MedicinebranchDTO medicinebranchDTO); 
    default Medicinebranch fromId(Long id) {
        if (id == null) {
            return null;
        }
        Medicinebranch medicinebranch = new Medicinebranch();
        medicinebranch.setId(id);
        return medicinebranch;
    }
}
