package com.skynet.accm.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Datasource entity.
 */
public class DatasourceDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    @NotNull
    private ZonedDateTime date;

    @Max(value = 10)
    private Integer turn;

    @Size(max = 50)
    private String personName;

    private Long actiontypeId;

    private String actiontypeName;

    private Long divisionId;

    private String divisionName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Integer getTurn() {
        return turn;
    }

    public void setTurn(Integer turn) {
        this.turn = turn;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Long getActiontypeId() {
        return actiontypeId;
    }

    public void setActiontypeId(Long actiontypeId) {
        this.actiontypeId = actiontypeId;
    }

    public String getActiontypeName() {
        return actiontypeName;
    }

    public void setActiontypeName(String actiontypeName) {
        this.actiontypeName = actiontypeName;
    }

    public Long getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Long divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DatasourceDTO datasourceDTO = (DatasourceDTO) o;
        if(datasourceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasourceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatasourceDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", date='" + getDate() + "'" +
            ", turn='" + getTurn() + "'" +
            ", personName='" + getPersonName() + "'" +
            "}";
    }
}
