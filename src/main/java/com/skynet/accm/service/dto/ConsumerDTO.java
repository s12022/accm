package com.skynet.accm.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.skynet.accm.domain.enumeration.Gender;
import com.skynet.accm.domain.enumeration.UserType;

/**
 * A DTO for the Consumer entity.
 */
public class ConsumerDTO implements Serializable {

    private Long id;

    @Size(min = 2, max = 100)
    private String firstName;

    @Size(min = 2, max = 100)
    private String lastName;

    private Gender gender;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")
    private String email;

    @NotNull
    private Boolean accepted;

    @NotNull
    private Boolean active;

    @Size(min = 9, max = 18)
    private String phoneNumber;

    private UserType userType;

    private String userComment;

    private Long addressId;

    private Set<ProductDTO> products = new HashSet<>();

    private Set<TherapyDTO> therapies = new HashSet<>();

    private Set<DiseaseDTO> diseases = new HashSet<>();

    private Set<MedicinebranchDTO> branches = new HashSet<>();

    private Set<ChildDTO> children = new HashSet<>();

    private Set<DatasourceDTO> datasources = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<TherapyDTO> getTherapies() {
        return therapies;
    }

    public void setTherapies(Set<TherapyDTO> therapies) {
        this.therapies = therapies;
    }

    public Set<DiseaseDTO> getDiseases() {
        return diseases;
    }

    public void setDiseases(Set<DiseaseDTO> diseases) {
        this.diseases = diseases;
    }

    public Set<MedicinebranchDTO> getBranches() {
        return branches;
    }

    public void setBranches(Set<MedicinebranchDTO> medicinebranches) {
        this.branches = medicinebranches;
    }

    public Set<ChildDTO> getChildren() {
        return children;
    }

    public void setChildren(Set<ChildDTO> children) {
        this.children = children;
    }

    public Set<DatasourceDTO> getDatasources() {
        return datasources;
    }

    public void setDatasources(Set<DatasourceDTO> datasources) {
        this.datasources = datasources;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConsumerDTO consumerDTO = (ConsumerDTO) o;
        if(consumerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consumerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConsumerDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", gender='" + getGender() + "'" +
            ", email='" + getEmail() + "'" +
            ", accepted='" + isAccepted() + "'" +
            ", active='" + isActive() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", userType='" + getUserType() + "'" +
            ", userComment='" + getUserComment() + "'" +
            "}";
    }
}
