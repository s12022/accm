package com.skynet.accm.repository;

import com.skynet.accm.domain.Division;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Division entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DivisionRepository extends JpaRepository<Division, Long> {

    @Query("select division from Division division left join fetch division.datasources where division.id =:id")
    Division findOneWithEagerRelationships(@Param("id") Long id);
}
