package com.skynet.accm.repository;

import com.skynet.accm.domain.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the Child entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChildRepository extends JpaRepository<Child, Long> {

    @Query("select child from Child child left join fetch child.consumers where child.id =:id")
    Child findOneWithEagerRelationships(@Param("id") Long id);


//    @Query("SELECT child FROM Child child left join fetch child.consumers")
//    Page<Child> getAllChildrenWithParents(Pageable pageable);
}
