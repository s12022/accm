package com.skynet.accm.repository;

import com.skynet.accm.domain.Disease;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Disease entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiseaseRepository extends JpaRepository<Disease, Long> {
    @Query("select distinct disease from Disease disease left join fetch disease.companies")
    List<Disease> findAllWithEagerRelationships();

    @Query("select disease from Disease disease left join fetch disease.companies left join fetch disease.consumers where disease.id =:id")
    Disease findOneWithEagerRelationships(@Param("id") Long id);

}
