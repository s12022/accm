package com.skynet.accm.repository;

import com.skynet.accm.domain.Product;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select product from Product product left join fetch product.consumers where product.id =:id")
    Product findOneWithEagerRelationships(@Param("id") Long id);
}
