package com.skynet.accm.repository;

import com.skynet.accm.domain.Actiontype;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Actiontype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActiontypeRepository extends JpaRepository<Actiontype, Long> {

}
