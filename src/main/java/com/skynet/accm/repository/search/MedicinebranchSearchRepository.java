package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Medicinebranch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Medicinebranch entity.
 */
public interface MedicinebranchSearchRepository extends ElasticsearchRepository<Medicinebranch, Long> {
}
