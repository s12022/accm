package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Child;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Child entity.
 */
public interface ChildSearchRepository extends ElasticsearchRepository<Child, Long> {
}
