package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Actiontype;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Actiontype entity.
 */
public interface ActiontypeSearchRepository extends ElasticsearchRepository<Actiontype, Long> {
}
