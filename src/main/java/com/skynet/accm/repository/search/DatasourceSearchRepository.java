package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Datasource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Datasource entity.
 */
public interface DatasourceSearchRepository extends ElasticsearchRepository<Datasource, Long> {
}
