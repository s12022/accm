package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Therapy;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Therapy entity.
 */
public interface TherapySearchRepository extends ElasticsearchRepository<Therapy, Long> {
}
