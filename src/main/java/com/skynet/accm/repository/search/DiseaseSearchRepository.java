package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Disease;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Disease entity.
 */
public interface DiseaseSearchRepository extends ElasticsearchRepository<Disease, Long> {
}
