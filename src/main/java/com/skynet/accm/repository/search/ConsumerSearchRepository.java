package com.skynet.accm.repository.search;

import com.skynet.accm.domain.Consumer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Consumer entity.
 */
public interface ConsumerSearchRepository extends ElasticsearchRepository<Consumer, Long> {
}
