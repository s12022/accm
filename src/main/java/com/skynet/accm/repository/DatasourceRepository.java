package com.skynet.accm.repository;

import com.skynet.accm.domain.Datasource;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Datasource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatasourceRepository extends JpaRepository<Datasource, Long> {

    @Query("select datasource from Datasource datasource left join fetch datasource.consumers where datasource.id =:id")
    Datasource findOneWithEagerRelationships(@Param("id") Long id);
}
