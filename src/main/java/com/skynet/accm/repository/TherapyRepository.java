package com.skynet.accm.repository;

import com.skynet.accm.domain.Therapy;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Therapy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TherapyRepository extends JpaRepository<Therapy, Long> {
    @Query("select distinct therapy from Therapy therapy left join fetch therapy.companies")
    List<Therapy> findAllWithEagerRelationships();

    @Query("select therapy from Therapy therapy left join fetch therapy.companies left join fetch therapy.consumers where therapy.id =:id")
    Therapy findOneWithEagerRelationships(@Param("id") Long id);

}
