package com.skynet.accm.repository;

import com.skynet.accm.domain.Medicinebranch;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Medicinebranch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedicinebranchRepository extends JpaRepository<Medicinebranch, Long> {
    @Query("select distinct medicinebranch from Medicinebranch medicinebranch left join fetch medicinebranch.companies")
    List<Medicinebranch> findAllWithEagerRelationships();

    @Query("select medicinebranch from Medicinebranch medicinebranch left join fetch medicinebranch.companies left join fetch medicinebranch.consumers where medicinebranch.id =:id")
    Medicinebranch findOneWithEagerRelationships(@Param("id") Long id);

}
