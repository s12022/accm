package com.skynet.accm.repository;

import com.skynet.accm.domain.Company;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("select company from Company company left join fetch company.therapies left join fetch company.diseases left join fetch company.branches where company.id =:id")
    Company findOneWithEagerRelationships(@Param("id") Long id);
}
