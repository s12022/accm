package com.skynet.accm.repository;

import com.skynet.accm.domain.Consumer;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Consumer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConsumerRepository extends JpaRepository<Consumer, Long> {
    @Query("select distinct consumer from Consumer consumer left join fetch consumer.products left join fetch consumer.therapies left join fetch consumer.diseases left join fetch consumer.branches left join fetch consumer.children left join fetch consumer.datasources")
    List<Consumer> findAllWithEagerRelationships();

    @Query("select consumer from Consumer consumer left join fetch consumer.products left join fetch consumer.therapies left join fetch consumer.diseases left join fetch consumer.branches left join fetch consumer.children left join fetch consumer.datasources where consumer.id =:id")
    Consumer findOneWithEagerRelationships(@Param("id") Long id);

}
