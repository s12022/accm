package com.skynet.accm.domain.enumeration;

/**
 * The UserType enumeration.
 */
public enum UserType {
    CONSUMER, SPECIALIST, OTHER
}
