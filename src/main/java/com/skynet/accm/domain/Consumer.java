package com.skynet.accm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.skynet.accm.domain.enumeration.Gender;

import com.skynet.accm.domain.enumeration.UserType;

/**
 * A Consumer.
 */
@Entity
@Table(name = "consumer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "consumer")
public class Consumer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 2, max = 100)
    @Column(name = "first_name", length = 100)
    private String firstName;

    @Size(min = 2, max = 100)
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @NotNull
    @Column(name = "accepted", nullable = false)
    private Boolean accepted;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @Size(min = 9, max = 18)
    @Column(name = "phone_number", length = 18)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type")
    private UserType userType;

    @Column(name = "user_comment")
    private String userComment;

    @ManyToOne
    private Address address;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "consumer_products",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="products_id", referencedColumnName="id"))
    private Set<Product> products = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "consumer_therapies",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="therapies_id", referencedColumnName="id"))
    private Set<Therapy> therapies = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "consumer_diseases",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="diseases_id", referencedColumnName="id"))
    private Set<Disease> diseases = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "consumer_branches",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="branches_id", referencedColumnName="id"))
    private Set<Medicinebranch> branches = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "consumer_children",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="children_id", referencedColumnName="id"))
    private Set<Child> children = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "consumer_datasources",
               joinColumns = @JoinColumn(name="consumers_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="datasources_id", referencedColumnName="id"))
    private Set<Datasource> datasources = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Consumer firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Consumer lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public Consumer gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public Consumer email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isAccepted() {
        return accepted;
    }

    public Consumer accepted(Boolean accepted) {
        this.accepted = accepted;
        return this;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Boolean isActive() {
        return active;
    }

    public Consumer active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Consumer phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserType getUserType() {
        return userType;
    }

    public Consumer userType(UserType userType) {
        this.userType = userType;
        return this;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUserComment() {
        return userComment;
    }

    public Consumer userComment(String userComment) {
        this.userComment = userComment;
        return this;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public Address getAddress() {
        return address;
    }

    public Consumer address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public Consumer products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public Consumer addProducts(Product product) {
        this.products.add(product);
        product.getConsumers().add(this);
        return this;
    }

    public Consumer removeProducts(Product product) {
        this.products.remove(product);
        product.getConsumers().remove(this);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Therapy> getTherapies() {
        return therapies;
    }

    public Consumer therapies(Set<Therapy> therapies) {
        this.therapies = therapies;
        return this;
    }

    public Consumer addTherapies(Therapy therapy) {
        this.therapies.add(therapy);
        therapy.getConsumers().add(this);
        return this;
    }

    public Consumer removeTherapies(Therapy therapy) {
        this.therapies.remove(therapy);
        therapy.getConsumers().remove(this);
        return this;
    }

    public void setTherapies(Set<Therapy> therapies) {
        this.therapies = therapies;
    }

    public Set<Disease> getDiseases() {
        return diseases;
    }

    public Consumer diseases(Set<Disease> diseases) {
        this.diseases = diseases;
        return this;
    }

    public Consumer addDiseases(Disease disease) {
        this.diseases.add(disease);
        disease.getConsumers().add(this);
        return this;
    }

    public Consumer removeDiseases(Disease disease) {
        this.diseases.remove(disease);
        disease.getConsumers().remove(this);
        return this;
    }

    public void setDiseases(Set<Disease> diseases) {
        this.diseases = diseases;
    }

    public Set<Medicinebranch> getBranches() {
        return branches;
    }

    public Consumer branches(Set<Medicinebranch> medicinebranches) {
        this.branches = medicinebranches;
        return this;
    }

    public Consumer addBranches(Medicinebranch medicinebranch) {
        this.branches.add(medicinebranch);
        medicinebranch.getConsumers().add(this);
        return this;
    }

    public Consumer removeBranches(Medicinebranch medicinebranch) {
        this.branches.remove(medicinebranch);
        medicinebranch.getConsumers().remove(this);
        return this;
    }

    public void setBranches(Set<Medicinebranch> medicinebranches) {
        this.branches = medicinebranches;
    }

    public Set<Child> getChildren() {
        return children;
    }

    public Consumer children(Set<Child> children) {
        this.children = children;
        return this;
    }

    public Consumer addChildren(Child child) {
        this.children.add(child);
        child.getConsumers().add(this);
        return this;
    }

    public Consumer removeChildren(Child child) {
        this.children.remove(child);
        child.getConsumers().remove(this);
        return this;
    }

    public void setChildren(Set<Child> children) {
        this.children = children;
    }

    public Set<Datasource> getDatasources() {
        return datasources;
    }

    public Consumer datasources(Set<Datasource> datasources) {
        this.datasources = datasources;
        return this;
    }

    public Consumer addDatasources(Datasource datasource) {
        this.datasources.add(datasource);
        datasource.getConsumers().add(this);
        return this;
    }

    public Consumer removeDatasources(Datasource datasource) {
        this.datasources.remove(datasource);
        datasource.getConsumers().remove(this);
        return this;
    }

    public void setDatasources(Set<Datasource> datasources) {
        this.datasources = datasources;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Consumer consumer = (Consumer) o;
        if (consumer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consumer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Consumer{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", gender='" + getGender() + "'" +
            ", email='" + getEmail() + "'" +
            ", accepted='" + isAccepted() + "'" +
            ", active='" + isActive() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", userType='" + getUserType() + "'" +
            ", userComment='" + getUserComment() + "'" +
            "}";
    }
}
