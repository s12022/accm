package com.skynet.accm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Therapy.
 */
@Entity
@Table(name = "therapy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "therapy")
public class Therapy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 150)
    @Column(name = "name", length = 150, nullable = false)
    private String name;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "therapy_companies",
               joinColumns = @JoinColumn(name="therapies_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="companies_id", referencedColumnName="id"))
    private Set<Company> companies = new HashSet<>();

    @ManyToMany(mappedBy = "therapies")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Consumer> consumers = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Therapy name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Company> getCompanies() {
        return companies;
    }

    public Therapy companies(Set<Company> companies) {
        this.companies = companies;
        return this;
    }

    public Therapy addCompanies(Company company) {
        this.companies.add(company);
        company.getTherapies().add(this);
        return this;
    }

    public Therapy removeCompanies(Company company) {
        this.companies.remove(company);
        company.getTherapies().remove(this);
        return this;
    }

    public void setCompanies(Set<Company> companies) {
        this.companies = companies;
    }

    public Set<Consumer> getConsumers() {
        return consumers;
    }

    public Therapy consumers(Set<Consumer> consumers) {
        this.consumers = consumers;
        return this;
    }

    public Therapy addConsumers(Consumer consumer) {
        this.consumers.add(consumer);
        consumer.getTherapies().add(this);
        return this;
    }

    public Therapy removeConsumers(Consumer consumer) {
        this.consumers.remove(consumer);
        consumer.getTherapies().remove(this);
        return this;
    }

    public void setConsumers(Set<Consumer> consumers) {
        this.consumers = consumers;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Therapy therapy = (Therapy) o;
        if (therapy.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), therapy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Therapy{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
