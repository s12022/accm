package com.skynet.accm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Datasource.
 */
@Entity
@Table(name = "datasource")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datasource")
public class Datasource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "jhi_date", nullable = false)
    private ZonedDateTime date;

    @Max(value = 10)
    @Column(name = "turn")
    private Integer turn;

    @Size(max = 50)
    @Column(name = "person_name", length = 50)
    private String personName;

    @ManyToOne(optional = false)
    @NotNull
    private Actiontype actiontype;

    @ManyToOne(optional = false)
    @NotNull
    private Division division;

    @ManyToMany(mappedBy = "datasources")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Consumer> consumers = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Datasource title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Datasource date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Integer getTurn() {
        return turn;
    }

    public Datasource turn(Integer turn) {
        this.turn = turn;
        return this;
    }

    public void setTurn(Integer turn) {
        this.turn = turn;
    }

    public String getPersonName() {
        return personName;
    }

    public Datasource personName(String personName) {
        this.personName = personName;
        return this;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Actiontype getActiontype() {
        return actiontype;
    }

    public Datasource actiontype(Actiontype actiontype) {
        this.actiontype = actiontype;
        return this;
    }

    public void setActiontype(Actiontype actiontype) {
        this.actiontype = actiontype;
    }

    public Division getDivision() {
        return division;
    }

    public Datasource division(Division division) {
        this.division = division;
        return this;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public Set<Consumer> getConsumers() {
        return consumers;
    }

    public Datasource consumers(Set<Consumer> consumers) {
        this.consumers = consumers;
        return this;
    }

    public Datasource addConsumers(Consumer consumer) {
        this.consumers.add(consumer);
        consumer.getDatasources().add(this);
        return this;
    }

    public Datasource removeConsumers(Consumer consumer) {
        this.consumers.remove(consumer);
        consumer.getDatasources().remove(this);
        return this;
    }

    public void setConsumers(Set<Consumer> consumers) {
        this.consumers = consumers;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Datasource datasource = (Datasource) o;
        if (datasource.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasource.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Datasource{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", date='" + getDate() + "'" +
            ", turn='" + getTurn() + "'" +
            ", personName='" + getPersonName() + "'" +
            "}";
    }
}
