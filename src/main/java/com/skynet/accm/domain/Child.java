package com.skynet.accm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Child.
 */
@Entity
@Table(name = "child")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "child")
public class Child implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 2, max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthdate")
    private ZonedDateTime birthdate;

    @Column(name = "parent_count", updatable = false, insertable = false, nullable = false)
    private Integer parentCount;

    @ManyToMany(mappedBy = "children")
    @Size(max = 2)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Consumer> consumers = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Child name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getBirthdate() {
        return birthdate;
    }

    public Child birthdate(ZonedDateTime birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public void setBirthdate(ZonedDateTime birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getParentCount() {
        return parentCount;
    }

    public Child parentCount(Integer parentCount) {
        this.parentCount = parentCount;
        return this;
    }

    public void setParentCount(Integer parentCount) {
        this.parentCount = parentCount;
    }

    public Set<Consumer> getConsumers() {
        return consumers;
    }

    public Child consumers(Set<Consumer> consumers) {
        this.consumers = consumers;
        return this;
    }

    public Child addConsumers(Consumer consumer) {
        this.consumers.add(consumer);
        consumer.getChildren().add(this);
        return this;
    }

    public Child removeConsumers(Consumer consumer) {
        this.consumers.remove(consumer);
        consumer.getChildren().remove(this);
        return this;
    }

    public void setConsumers(Set<Consumer> consumers) {
        this.consumers = consumers;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Child child = (Child) o;
        if (child.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), child.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Child{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", birthdate='" + getBirthdate() + "'" +
            ", parentCount='" + getParentCount() + "'" +
            "}";
    }
}
