package com.skynet.accm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Company.
 */
@Entity
@Table(name = "company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "company")
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 150)
    @Column(name = "name", length = 150, nullable = false)
    private String name;

    @OneToMany(mappedBy = "company")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Division> divisions = new HashSet<>();

    @OneToMany(mappedBy = "company")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Product> products = new HashSet<>();

    @ManyToMany(mappedBy = "companies")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Therapy> therapies = new HashSet<>();

    @ManyToMany(mappedBy = "companies")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Disease> diseases = new HashSet<>();

    @ManyToMany(mappedBy = "companies")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Medicinebranch> branches = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Company name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Division> getDivisions() {
        return divisions;
    }

    public Company divisions(Set<Division> divisions) {
        this.divisions = divisions;
        return this;
    }

    public Company addDivisions(Division division) {
        this.divisions.add(division);
        division.setCompany(this);
        return this;
    }

    public Company removeDivisions(Division division) {
        this.divisions.remove(division);
        division.setCompany(null);
        return this;
    }

    public void setDivisions(Set<Division> divisions) {
        this.divisions = divisions;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public Company products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public Company addProducts(Product product) {
        this.products.add(product);
        product.setCompany(this);
        return this;
    }

    public Company removeProducts(Product product) {
        this.products.remove(product);
        product.setCompany(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Therapy> getTherapies() {
        return therapies;
    }

    public Company therapies(Set<Therapy> therapies) {
        this.therapies = therapies;
        return this;
    }

    public Company addTherapies(Therapy therapy) {
        this.therapies.add(therapy);
        therapy.getCompanies().add(this);
        return this;
    }

    public Company removeTherapies(Therapy therapy) {
        this.therapies.remove(therapy);
        therapy.getCompanies().remove(this);
        return this;
    }

    public void setTherapies(Set<Therapy> therapies) {
        this.therapies = therapies;
    }

    public Set<Disease> getDiseases() {
        return diseases;
    }

    public Company diseases(Set<Disease> diseases) {
        this.diseases = diseases;
        return this;
    }

    public Company addDiseases(Disease disease) {
        this.diseases.add(disease);
        disease.getCompanies().add(this);
        return this;
    }

    public Company removeDiseases(Disease disease) {
        this.diseases.remove(disease);
        disease.getCompanies().remove(this);
        return this;
    }

    public void setDiseases(Set<Disease> diseases) {
        this.diseases = diseases;
    }

    public Set<Medicinebranch> getBranches() {
        return branches;
    }

    public Company branches(Set<Medicinebranch> medicinebranches) {
        this.branches = medicinebranches;
        return this;
    }

    public Company addBranches(Medicinebranch medicinebranch) {
        this.branches.add(medicinebranch);
        medicinebranch.getCompanies().add(this);
        return this;
    }

    public Company removeBranches(Medicinebranch medicinebranch) {
        this.branches.remove(medicinebranch);
        medicinebranch.getCompanies().remove(this);
        return this;
    }

    public void setBranches(Set<Medicinebranch> medicinebranches) {
        this.branches = medicinebranches;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Company company = (Company) o;
        if (company.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), company.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Company{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
