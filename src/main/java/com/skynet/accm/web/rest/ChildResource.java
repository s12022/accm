package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Child;

import com.skynet.accm.repository.ChildRepository;
import com.skynet.accm.repository.search.ChildSearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Child.
 */
@RestController
@RequestMapping("/api")
public class ChildResource {

    private final Logger log = LoggerFactory.getLogger(ChildResource.class);

    private static final String ENTITY_NAME = "child";

    private final ChildRepository childRepository;

    private final ChildSearchRepository childSearchRepository;
    public ChildResource(ChildRepository childRepository, ChildSearchRepository childSearchRepository) {
        this.childRepository = childRepository;
        this.childSearchRepository = childSearchRepository;
    }

    /**
     * POST  /children : Create a new child.
     *
     * @param child the child to create
     * @return the ResponseEntity with status 201 (Created) and with body the new child, or with status 400 (Bad Request) if the child has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/children")
    @Timed
    public ResponseEntity<Child> createChild(@Valid @RequestBody Child child) throws URISyntaxException {
        log.debug("REST request to save Child : {}", child);
        if (child.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new child cannot already have an ID")).body(null);
        }
        Child result = childRepository.save(child);
        childSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/children/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /children : Updates an existing child.
     *
     * @param child the child to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated child,
     * or with status 400 (Bad Request) if the child is not valid,
     * or with status 500 (Internal Server Error) if the child couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/children")
    @Timed
    public ResponseEntity<Child> updateChild(@Valid @RequestBody Child child) throws URISyntaxException {
        log.debug("REST request to update Child : {}", child);
        if (child.getId() == null) {
            return createChild(child);
        }
        Child result = childRepository.save(child);
        childSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, child.getId().toString()))
            .body(result);
    }

    /**
     * GET  /children : get all the children.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of children in body
     */
    @GetMapping("/children")
    @Timed
    public ResponseEntity<List<Child>> getAllChildren(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Children");
        Page<Child> page = childRepository.findAll(new PageRequest(0, Integer.MAX_VALUE));
//        Page<Child> page = childRepository.getAllChildrenWithParents(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/children");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /children/:id : get the "id" child.
     *
     * @param id the id of the child to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the child, or with status 404 (Not Found)
     */
    @GetMapping("/children/{id}")
    @Timed
    public ResponseEntity<Child> getChild(@PathVariable Long id) {
        log.debug("REST request to get Child : {}", id);
        Child child = childRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(child));
    }

    /**
     * DELETE  /children/:id : delete the "id" child.
     *
     * @param id the id of the child to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/children/{id}")
    @Timed
    public ResponseEntity<Void> deleteChild(@PathVariable Long id) {
        log.debug("REST request to delete Child : {}", id);
        childRepository.delete(id);
        childSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/children?query=:query : search for the child corresponding
     * to the query.
     *
     * @param query the query of the child search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/children")
    @Timed
    public ResponseEntity<List<Child>> searchChildren(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Children for query {}", query);
        Page<Child> page = childSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/children");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
