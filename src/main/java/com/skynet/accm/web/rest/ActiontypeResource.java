package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Actiontype;

import com.skynet.accm.repository.ActiontypeRepository;
import com.skynet.accm.repository.search.ActiontypeSearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.service.dto.ActiontypeDTO;
import com.skynet.accm.service.mapper.ActiontypeMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Actiontype.
 */
@RestController
@RequestMapping("/api")
public class ActiontypeResource {

    private final Logger log = LoggerFactory.getLogger(ActiontypeResource.class);

    private static final String ENTITY_NAME = "actiontype";

    private final ActiontypeRepository actiontypeRepository;

    private final ActiontypeMapper actiontypeMapper;

    private final ActiontypeSearchRepository actiontypeSearchRepository;
    public ActiontypeResource(ActiontypeRepository actiontypeRepository, ActiontypeMapper actiontypeMapper, ActiontypeSearchRepository actiontypeSearchRepository) {
        this.actiontypeRepository = actiontypeRepository;
        this.actiontypeMapper = actiontypeMapper;
        this.actiontypeSearchRepository = actiontypeSearchRepository;
    }

    /**
     * POST  /actiontypes : Create a new actiontype.
     *
     * @param actiontypeDTO the actiontypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new actiontypeDTO, or with status 400 (Bad Request) if the actiontype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/actiontypes")
    @Timed
    public ResponseEntity<ActiontypeDTO> createActiontype(@Valid @RequestBody ActiontypeDTO actiontypeDTO) throws URISyntaxException {
        log.debug("REST request to save Actiontype : {}", actiontypeDTO);
        if (actiontypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new actiontype cannot already have an ID")).body(null);
        }
        Actiontype actiontype = actiontypeMapper.toEntity(actiontypeDTO);
        actiontype = actiontypeRepository.save(actiontype);
        ActiontypeDTO result = actiontypeMapper.toDto(actiontype);
        actiontypeSearchRepository.save(actiontype);
        return ResponseEntity.created(new URI("/api/actiontypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /actiontypes : Updates an existing actiontype.
     *
     * @param actiontypeDTO the actiontypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated actiontypeDTO,
     * or with status 400 (Bad Request) if the actiontypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the actiontypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/actiontypes")
    @Timed
    public ResponseEntity<ActiontypeDTO> updateActiontype(@Valid @RequestBody ActiontypeDTO actiontypeDTO) throws URISyntaxException {
        log.debug("REST request to update Actiontype : {}", actiontypeDTO);
        if (actiontypeDTO.getId() == null) {
            return createActiontype(actiontypeDTO);
        }
        Actiontype actiontype = actiontypeMapper.toEntity(actiontypeDTO);
        actiontype = actiontypeRepository.save(actiontype);
        ActiontypeDTO result = actiontypeMapper.toDto(actiontype);
        actiontypeSearchRepository.save(actiontype);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, actiontypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /actiontypes : get all the actiontypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of actiontypes in body
     */
    @GetMapping("/actiontypes")
    @Timed
    public List<ActiontypeDTO> getAllActiontypes() {
        log.debug("REST request to get all Actiontypes");
        List<Actiontype> actiontypes = actiontypeRepository.findAll();
        return actiontypeMapper.toDto(actiontypes);
        }

    /**
     * GET  /actiontypes/:id : get the "id" actiontype.
     *
     * @param id the id of the actiontypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the actiontypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/actiontypes/{id}")
    @Timed
    public ResponseEntity<ActiontypeDTO> getActiontype(@PathVariable Long id) {
        log.debug("REST request to get Actiontype : {}", id);
        Actiontype actiontype = actiontypeRepository.findOne(id);
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(actiontype);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(actiontypeDTO));
    }

    /**
     * DELETE  /actiontypes/:id : delete the "id" actiontype.
     *
     * @param id the id of the actiontypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/actiontypes/{id}")
    @Timed
    public ResponseEntity<Void> deleteActiontype(@PathVariable Long id) {
        log.debug("REST request to delete Actiontype : {}", id);
        actiontypeRepository.delete(id);
        actiontypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/actiontypes?query=:query : search for the actiontype corresponding
     * to the query.
     *
     * @param query the query of the actiontype search
     * @return the result of the search
     */
    @GetMapping("/_search/actiontypes")
    @Timed
    public List<ActiontypeDTO> searchActiontypes(@RequestParam String query) {
        log.debug("REST request to search Actiontypes for query {}", query);
        return StreamSupport
            .stream(actiontypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(actiontypeMapper::toDto)
            .collect(Collectors.toList());
    }

}
