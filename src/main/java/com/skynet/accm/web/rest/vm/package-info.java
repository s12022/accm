/**
 * View Models used by Spring MVC REST controllers.
 */
package com.skynet.accm.web.rest.vm;
