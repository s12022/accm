package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Medicinebranch;

import com.skynet.accm.repository.MedicinebranchRepository;
import com.skynet.accm.repository.search.MedicinebranchSearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Medicinebranch.
 */
@RestController
@RequestMapping("/api")
public class MedicinebranchResource {

    private final Logger log = LoggerFactory.getLogger(MedicinebranchResource.class);

    private static final String ENTITY_NAME = "medicinebranch";

    private final MedicinebranchRepository medicinebranchRepository;

    private final MedicinebranchSearchRepository medicinebranchSearchRepository;
    public MedicinebranchResource(MedicinebranchRepository medicinebranchRepository, MedicinebranchSearchRepository medicinebranchSearchRepository) {
        this.medicinebranchRepository = medicinebranchRepository;
        this.medicinebranchSearchRepository = medicinebranchSearchRepository;
    }

    /**
     * POST  /medicinebranches : Create a new medicinebranch.
     *
     * @param medicinebranch the medicinebranch to create
     * @return the ResponseEntity with status 201 (Created) and with body the new medicinebranch, or with status 400 (Bad Request) if the medicinebranch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/medicinebranches")
    @Timed
    public ResponseEntity<Medicinebranch> createMedicinebranch(@Valid @RequestBody Medicinebranch medicinebranch) throws URISyntaxException {
        log.debug("REST request to save Medicinebranch : {}", medicinebranch);
        if (medicinebranch.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new medicinebranch cannot already have an ID")).body(null);
        }
        Medicinebranch result = medicinebranchRepository.save(medicinebranch);
        medicinebranchSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/medicinebranches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /medicinebranches : Updates an existing medicinebranch.
     *
     * @param medicinebranch the medicinebranch to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated medicinebranch,
     * or with status 400 (Bad Request) if the medicinebranch is not valid,
     * or with status 500 (Internal Server Error) if the medicinebranch couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/medicinebranches")
    @Timed
    public ResponseEntity<Medicinebranch> updateMedicinebranch(@Valid @RequestBody Medicinebranch medicinebranch) throws URISyntaxException {
        log.debug("REST request to update Medicinebranch : {}", medicinebranch);
        if (medicinebranch.getId() == null) {
            return createMedicinebranch(medicinebranch);
        }
        Medicinebranch result = medicinebranchRepository.save(medicinebranch);
        medicinebranchSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, medicinebranch.getId().toString()))
            .body(result);
    }

    /**
     * GET  /medicinebranches : get all the medicinebranches.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of medicinebranches in body
     */
    @GetMapping("/medicinebranches")
    @Timed
    public ResponseEntity<List<Medicinebranch>> getAllMedicinebranches(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Medicinebranches");
        Page<Medicinebranch> page = medicinebranchRepository.findAll(new PageRequest(0, Integer.MAX_VALUE));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/medicinebranches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /medicinebranches/:id : get the "id" medicinebranch.
     *
     * @param id the id of the medicinebranch to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the medicinebranch, or with status 404 (Not Found)
     */
    @GetMapping("/medicinebranches/{id}")
    @Timed
    public ResponseEntity<Medicinebranch> getMedicinebranch(@PathVariable Long id) {
        log.debug("REST request to get Medicinebranch : {}", id);
        Medicinebranch medicinebranch = medicinebranchRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(medicinebranch));
    }

    /**
     * DELETE  /medicinebranches/:id : delete the "id" medicinebranch.
     *
     * @param id the id of the medicinebranch to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/medicinebranches/{id}")
    @Timed
    public ResponseEntity<Void> deleteMedicinebranch(@PathVariable Long id) {
        log.debug("REST request to delete Medicinebranch : {}", id);
        medicinebranchRepository.delete(id);
        medicinebranchSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/medicinebranches?query=:query : search for the medicinebranch corresponding
     * to the query.
     *
     * @param query the query of the medicinebranch search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/medicinebranches")
    @Timed
    public ResponseEntity<List<Medicinebranch>> searchMedicinebranches(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Medicinebranches for query {}", query);
        Page<Medicinebranch> page = medicinebranchSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/medicinebranches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
