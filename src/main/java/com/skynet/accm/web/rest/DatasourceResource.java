package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Datasource;

import com.skynet.accm.repository.DatasourceRepository;
import com.skynet.accm.repository.search.DatasourceSearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.service.dto.DatasourceDTO;
import com.skynet.accm.service.mapper.DatasourceMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Datasource.
 */
@RestController
@RequestMapping("/api")
public class DatasourceResource {

    private final Logger log = LoggerFactory.getLogger(DatasourceResource.class);

    private static final String ENTITY_NAME = "datasource";

    private final DatasourceRepository datasourceRepository;

    private final DatasourceMapper datasourceMapper;

    private final DatasourceSearchRepository datasourceSearchRepository;
    public DatasourceResource(DatasourceRepository datasourceRepository, DatasourceMapper datasourceMapper, DatasourceSearchRepository datasourceSearchRepository) {
        this.datasourceRepository = datasourceRepository;
        this.datasourceMapper = datasourceMapper;
        this.datasourceSearchRepository = datasourceSearchRepository;
    }

    /**
     * POST  /datasources : Create a new datasource.
     *
     * @param datasourceDTO the datasourceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datasourceDTO, or with status 400 (Bad Request) if the datasource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/datasources")
    @Timed
    public ResponseEntity<DatasourceDTO> createDatasource(@Valid @RequestBody DatasourceDTO datasourceDTO) throws URISyntaxException {
        log.debug("REST request to save Datasource : {}", datasourceDTO);
        if (datasourceDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new datasource cannot already have an ID")).body(null);
        }
        Datasource datasource = datasourceMapper.toEntity(datasourceDTO);
        datasource = datasourceRepository.save(datasource);
        DatasourceDTO result = datasourceMapper.toDto(datasource);
        datasourceSearchRepository.save(datasource);
        return ResponseEntity.created(new URI("/api/datasources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /datasources : Updates an existing datasource.
     *
     * @param datasourceDTO the datasourceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datasourceDTO,
     * or with status 400 (Bad Request) if the datasourceDTO is not valid,
     * or with status 500 (Internal Server Error) if the datasourceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/datasources")
    @Timed
    public ResponseEntity<DatasourceDTO> updateDatasource(@Valid @RequestBody DatasourceDTO datasourceDTO) throws URISyntaxException {
        log.debug("REST request to update Datasource : {}", datasourceDTO);
        if (datasourceDTO.getId() == null) {
            return createDatasource(datasourceDTO);
        }
        Datasource datasource = datasourceMapper.toEntity(datasourceDTO);
        datasource = datasourceRepository.save(datasource);
        DatasourceDTO result = datasourceMapper.toDto(datasource);
        datasourceSearchRepository.save(datasource);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, datasourceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /datasources : get all the datasources.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of datasources in body
     */
    @GetMapping("/datasources")
    @Timed
    public List<DatasourceDTO> getAllDatasources() {
        log.debug("REST request to get all Datasources");
        List<Datasource> datasources = datasourceRepository.findAll();
        return datasourceMapper.toDto(datasources);
        }

    /**
     * GET  /datasources/:id : get the "id" datasource.
     *
     * @param id the id of the datasourceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datasourceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/datasources/{id}")
    @Timed
    public ResponseEntity<DatasourceDTO> getDatasource(@PathVariable Long id) {
        log.debug("REST request to get Datasource : {}", id);
        Datasource datasource = datasourceRepository.findOneWithEagerRelationships(id);
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(datasourceDTO));
    }

    /**
     * DELETE  /datasources/:id : delete the "id" datasource.
     *
     * @param id the id of the datasourceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/datasources/{id}")
    @Timed
    public ResponseEntity<Void> deleteDatasource(@PathVariable Long id) {
        log.debug("REST request to delete Datasource : {}", id);
        datasourceRepository.delete(id);
        datasourceSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/datasources?query=:query : search for the datasource corresponding
     * to the query.
     *
     * @param query the query of the datasource search
     * @return the result of the search
     */
    @GetMapping("/_search/datasources")
    @Timed
    public List<DatasourceDTO> searchDatasources(@RequestParam String query) {
        log.debug("REST request to search Datasources for query {}", query);
        return StreamSupport
            .stream(datasourceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(datasourceMapper::toDto)
            .collect(Collectors.toList());
    }

}
