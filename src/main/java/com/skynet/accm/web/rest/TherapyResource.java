package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Therapy;

import com.skynet.accm.repository.TherapyRepository;
import com.skynet.accm.repository.search.TherapySearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Therapy.
 */
@RestController
@RequestMapping("/api")
public class TherapyResource {

    private final Logger log = LoggerFactory.getLogger(TherapyResource.class);

    private static final String ENTITY_NAME = "therapy";

    private final TherapyRepository therapyRepository;

    private final TherapySearchRepository therapySearchRepository;
    public TherapyResource(TherapyRepository therapyRepository, TherapySearchRepository therapySearchRepository) {
        this.therapyRepository = therapyRepository;
        this.therapySearchRepository = therapySearchRepository;
    }

    /**
     * POST  /therapies : Create a new therapy.
     *
     * @param therapy the therapy to create
     * @return the ResponseEntity with status 201 (Created) and with body the new therapy, or with status 400 (Bad Request) if the therapy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/therapies")
    @Timed
    public ResponseEntity<Therapy> createTherapy(@Valid @RequestBody Therapy therapy) throws URISyntaxException {
        log.debug("REST request to save Therapy : {}", therapy);
        if (therapy.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new therapy cannot already have an ID")).body(null);
        }
        Therapy result = therapyRepository.save(therapy);
        therapySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/therapies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /therapies : Updates an existing therapy.
     *
     * @param therapy the therapy to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated therapy,
     * or with status 400 (Bad Request) if the therapy is not valid,
     * or with status 500 (Internal Server Error) if the therapy couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/therapies")
    @Timed
    public ResponseEntity<Therapy> updateTherapy(@Valid @RequestBody Therapy therapy) throws URISyntaxException {
        log.debug("REST request to update Therapy : {}", therapy);
        if (therapy.getId() == null) {
            return createTherapy(therapy);
        }
        Therapy result = therapyRepository.save(therapy);
        therapySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, therapy.getId().toString()))
            .body(result);
    }

    /**
     * GET  /therapies : get all the therapies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of therapies in body
     */
    @GetMapping("/therapies")
    @Timed
    public ResponseEntity<List<Therapy>> getAllTherapies(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Therapies");
        Page<Therapy> page = therapyRepository.findAll(new PageRequest(0, Integer.MAX_VALUE));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/therapies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /therapies/:id : get the "id" therapy.
     *
     * @param id the id of the therapy to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the therapy, or with status 404 (Not Found)
     */
    @GetMapping("/therapies/{id}")
    @Timed
    public ResponseEntity<Therapy> getTherapy(@PathVariable Long id) {
        log.debug("REST request to get Therapy : {}", id);
        Therapy therapy = therapyRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(therapy));
    }

    /**
     * DELETE  /therapies/:id : delete the "id" therapy.
     *
     * @param id the id of the therapy to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/therapies/{id}")
    @Timed
    public ResponseEntity<Void> deleteTherapy(@PathVariable Long id) {
        log.debug("REST request to delete Therapy : {}", id);
        therapyRepository.delete(id);
        therapySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/therapies?query=:query : search for the therapy corresponding
     * to the query.
     *
     * @param query the query of the therapy search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/therapies")
    @Timed
    public ResponseEntity<List<Therapy>> searchTherapies(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Therapies for query {}", query);
        Page<Therapy> page = therapySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/therapies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
