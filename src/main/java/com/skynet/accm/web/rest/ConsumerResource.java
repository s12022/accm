package com.skynet.accm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.skynet.accm.domain.Consumer;

import com.skynet.accm.repository.ConsumerRepository;
import com.skynet.accm.repository.search.ConsumerSearchRepository;
import com.skynet.accm.web.rest.util.HeaderUtil;
import com.skynet.accm.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Consumer.
 */
@RestController
@RequestMapping("/api")
public class ConsumerResource {

    private final Logger log = LoggerFactory.getLogger(ConsumerResource.class);

    private static final String ENTITY_NAME = "consumer";

    private final ConsumerRepository consumerRepository;

    private final ConsumerSearchRepository consumerSearchRepository;
    public ConsumerResource(ConsumerRepository consumerRepository, ConsumerSearchRepository consumerSearchRepository) {
        this.consumerRepository = consumerRepository;
        this.consumerSearchRepository = consumerSearchRepository;
    }

    /**
     * POST  /consumers : Create a new consumer.
     *
     * @param consumer the consumer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consumer, or with status 400 (Bad Request) if the consumer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/consumers")
    @Timed
    public ResponseEntity<Consumer> createConsumer(@Valid @RequestBody Consumer consumer) throws URISyntaxException {
        log.debug("REST request to save Consumer : {}", consumer);
        if (consumer.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new consumer cannot already have an ID")).body(null);
        }
        Consumer result = consumerRepository.save(consumer);
        consumerSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/consumers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consumers : Updates an existing consumer.
     *
     * @param consumer the consumer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consumer,
     * or with status 400 (Bad Request) if the consumer is not valid,
     * or with status 500 (Internal Server Error) if the consumer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/consumers")
    @Timed
    public ResponseEntity<Consumer> updateConsumer(@Valid @RequestBody Consumer consumer) throws URISyntaxException {
        log.debug("REST request to update Consumer : {}", consumer);
        if (consumer.getId() == null) {
            return createConsumer(consumer);
        }
        Consumer result = consumerRepository.save(consumer);
        consumerSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, consumer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consumers : get all the consumers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of consumers in body
     */
    @GetMapping("/consumers")
    @Timed
    public ResponseEntity<List<Consumer>> getAllConsumers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Consumers");
        Page<Consumer> page = consumerRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/consumers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /consumers/:id : get the "id" consumer.
     *
     * @param id the id of the consumer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consumer, or with status 404 (Not Found)
     */
    @GetMapping("/consumers/{id}")
    @Timed
    public ResponseEntity<Consumer> getConsumer(@PathVariable Long id) {
        log.debug("REST request to get Consumer : {}", id);
        Consumer consumer = consumerRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(consumer));
    }

    /**
     * DELETE  /consumers/:id : delete the "id" consumer.
     *
     * @param id the id of the consumer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/consumers/{id}")
    @Timed
    public ResponseEntity<Void> deleteConsumer(@PathVariable Long id) {
        log.debug("REST request to delete Consumer : {}", id);
        consumerRepository.delete(id);
        consumerSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/consumers?query=:query : search for the consumer corresponding
     * to the query.
     *
     * @param query the query of the consumer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/consumers")
    @Timed
    public ResponseEntity<List<Consumer>> searchConsumers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Consumers for query {}", query);
        Page<Consumer> page = consumerSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/consumers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
