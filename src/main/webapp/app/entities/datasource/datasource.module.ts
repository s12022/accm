import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccmSharedModule } from '../../shared';
import {
    DatasourceService,
    DatasourcePopupService,
    DatasourceComponent,
    DatasourceDetailComponent,
    DatasourceDialogComponent,
    DatasourcePopupComponent,
    DatasourceDeletePopupComponent,
    DatasourceDeleteDialogComponent,
    datasourceRoute,
    datasourcePopupRoute,
} from './';

const ENTITY_STATES = [
    ...datasourceRoute,
    ...datasourcePopupRoute,
];

@NgModule({
    imports: [
        AccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DatasourceComponent,
        DatasourceDetailComponent,
        DatasourceDialogComponent,
        DatasourceDeleteDialogComponent,
        DatasourcePopupComponent,
        DatasourceDeletePopupComponent,
    ],
    entryComponents: [
        DatasourceComponent,
        DatasourceDialogComponent,
        DatasourcePopupComponent,
        DatasourceDeleteDialogComponent,
        DatasourceDeletePopupComponent,
    ],
    providers: [
        DatasourceService,
        DatasourcePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmDatasourceModule {}
