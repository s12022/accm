import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Datasource } from './datasource.model';
import { DatasourcePopupService } from './datasource-popup.service';
import { DatasourceService } from './datasource.service';
import { Actiontype, ActiontypeService } from '../actiontype';
import { Division, DivisionService } from '../division';
import { Consumer, ConsumerService } from '../consumer';
import { ResponseWrapper } from '../../shared';
import { Subscription } from 'rxjs/Rx';

@Component({
    selector: 'jhi-datasource-dialog',
    templateUrl: './datasource-dialog.component.html'
})
export class DatasourceDialogComponent implements OnInit {

    datasource: Datasource;
    isSaving: boolean;

    actiontypes: Actiontype[];

    divisions: Division[];

    consumers: Consumer[];

    actiontypeEventSubscriber: Subscription;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private datasourceService: DatasourceService,
        private actiontypeService: ActiontypeService,
        private divisionService: DivisionService,
        private consumerService: ConsumerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.actiontypeService.query()
            .subscribe((res: ResponseWrapper) => { this.actiontypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.divisionService.query()
            .subscribe((res: ResponseWrapper) => { this.divisions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.consumerService.query()
            .subscribe((res: ResponseWrapper) => { this.consumers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.actiontypeEventSubscriber = this.eventManager
            .subscribe('actiontypeListModification', (response) => this.loadActionTypes());
    }

    loadActionTypes() {
        this.actiontypeService.query()
            .subscribe((res: ResponseWrapper) => { this.actiontypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.datasource.id !== undefined) {
            this.subscribeToSaveResponse(
                this.datasourceService.update(this.datasource));
        } else {
            this.subscribeToSaveResponse(
                this.datasourceService.create(this.datasource));
        }
    }

    private subscribeToSaveResponse(result: Observable<Datasource>) {
        result.subscribe((res: Datasource) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Datasource) {
        this.eventManager.broadcast({ name: 'datasourceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackActiontypeById(index: number, item: Actiontype) {
        return item.id;
    }

    trackDivisionById(index: number, item: Division) {
        return item.id;
    }

    trackConsumerById(index: number, item: Consumer) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-datasource-popup',
    template: ''
})
export class DatasourcePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private datasourcePopupService: DatasourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.datasourcePopupService
                    .open(DatasourceDialogComponent as Component, params['id']);
            } else {
                this.datasourcePopupService
                    .open(DatasourceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
