import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DatasourceComponent } from './datasource.component';
import { DatasourceDetailComponent } from './datasource-detail.component';
import { DatasourcePopupComponent } from './datasource-dialog.component';
import { DatasourceDeletePopupComponent } from './datasource-delete-dialog.component';

export const datasourceRoute: Routes = [
    {
        path: 'datasource',
        component: DatasourceComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'datasource/:id',
        component: DatasourceDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const datasourcePopupRoute: Routes = [
    {
        path: 'datasource-new',
        component: DatasourcePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
{
        path: 'datasource-new-from-consumer',
        component: DatasourcePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'datasource/:id/edit',
        component: DatasourcePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'datasource/:id/delete',
        component: DatasourceDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
