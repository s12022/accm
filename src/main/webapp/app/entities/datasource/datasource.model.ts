import { BaseEntity } from './../../shared';

export class Datasource implements BaseEntity {
    constructor(
        public id?: number,
        public title?: string,
        public date?: any,
        public turn?: number,
        public personName?: string,
        public actiontypeId?: number,
        public divisionId?: number,
        public consumers?: BaseEntity[],
    ) {
    }
}
