import { BaseEntity } from './../../shared';

export class Division implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public companyId?: number,
        public datasources?: BaseEntity[],
    ) {
    }
}
