import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Actiontype } from './actiontype.model';
import { ActiontypeService } from './actiontype.service';

@Component({
    selector: 'jhi-actiontype-detail',
    templateUrl: './actiontype-detail.component.html'
})
export class ActiontypeDetailComponent implements OnInit, OnDestroy {

    actiontype: Actiontype;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private actiontypeService: ActiontypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInActiontypes();
    }

    load(id) {
        this.actiontypeService.find(id).subscribe((actiontype) => {
            this.actiontype = actiontype;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInActiontypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'actiontypeListModification',
            (response) => this.load(this.actiontype.id)
        );
    }
}
