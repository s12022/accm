import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Actiontype } from './actiontype.model';
import { ActiontypePopupService } from './actiontype-popup.service';
import { ActiontypeService } from './actiontype.service';

@Component({
    selector: 'jhi-actiontype-delete-dialog',
    templateUrl: './actiontype-delete-dialog.component.html'
})
export class ActiontypeDeleteDialogComponent {

    actiontype: Actiontype;

    constructor(
        private actiontypeService: ActiontypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.actiontypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'actiontypeListModification',
                content: 'Deleted an actiontype'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-actiontype-delete-popup',
    template: ''
})
export class ActiontypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private actiontypePopupService: ActiontypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.actiontypePopupService
                .open(ActiontypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
