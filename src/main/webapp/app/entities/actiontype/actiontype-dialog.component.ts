import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Actiontype } from './actiontype.model';
import { ActiontypePopupService } from './actiontype-popup.service';
import { ActiontypeService } from './actiontype.service';

@Component({
    selector: 'jhi-actiontype-dialog',
    templateUrl: './actiontype-dialog.component.html'
})
export class ActiontypeDialogComponent implements OnInit {

    actiontype: Actiontype;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private actiontypeService: ActiontypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.actiontype.id !== undefined) {
            this.subscribeToSaveResponse(
                this.actiontypeService.update(this.actiontype));
        } else {
            this.subscribeToSaveResponse(
                this.actiontypeService.create(this.actiontype));
        }
    }

    private subscribeToSaveResponse(result: Observable<Actiontype>) {
        result.subscribe((res: Actiontype) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Actiontype) {
        this.eventManager.broadcast({ name: 'actiontypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-actiontype-popup',
    template: ''
})
export class ActiontypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private actiontypePopupService: ActiontypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.actiontypePopupService
                    .open(ActiontypeDialogComponent as Component, params['id']);
            } else {
                this.actiontypePopupService
                    .open(ActiontypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
