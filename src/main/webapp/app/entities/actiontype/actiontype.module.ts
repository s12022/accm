import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccmSharedModule } from '../../shared';
import {
    ActiontypeService,
    ActiontypePopupService,
    ActiontypeComponent,
    ActiontypeDetailComponent,
    ActiontypeDialogComponent,
    ActiontypePopupComponent,
    ActiontypeDeletePopupComponent,
    ActiontypeDeleteDialogComponent,
    actiontypeRoute,
    actiontypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...actiontypeRoute,
    ...actiontypePopupRoute,
];

@NgModule({
    imports: [
        AccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ActiontypeComponent,
        ActiontypeDetailComponent,
        ActiontypeDialogComponent,
        ActiontypeDeleteDialogComponent,
        ActiontypePopupComponent,
        ActiontypeDeletePopupComponent,
    ],
    entryComponents: [
        ActiontypeComponent,
        ActiontypeDialogComponent,
        ActiontypePopupComponent,
        ActiontypeDeleteDialogComponent,
        ActiontypeDeletePopupComponent,
    ],
    providers: [
        ActiontypeService,
        ActiontypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmActiontypeModule {}
