import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ActiontypeComponent } from './actiontype.component';
import { ActiontypeDetailComponent } from './actiontype-detail.component';
import { ActiontypePopupComponent } from './actiontype-dialog.component';
import { ActiontypeDeletePopupComponent } from './actiontype-delete-dialog.component';

export const actiontypeRoute: Routes = [
    {
        path: 'actiontype',
        component: ActiontypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.actiontype.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'actiontype/:id',
        component: ActiontypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.actiontype.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const actiontypePopupRoute: Routes = [
    {
        path: 'actiontype-new',
        component: ActiontypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.actiontype.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'actiontype-new-from-datasource',
        component: ActiontypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.datasource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'actiontype/:id/edit',
        component: ActiontypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.actiontype.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'actiontype/:id/delete',
        component: ActiontypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.actiontype.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
