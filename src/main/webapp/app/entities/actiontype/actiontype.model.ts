import { BaseEntity } from './../../shared';

export class Actiontype implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
