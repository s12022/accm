import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Therapy } from './therapy.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TherapyService {

    private resourceUrl = 'api/therapies';
    private resourceSearchUrl = 'api/_search/therapies';

    constructor(private http: Http) { }

    create(therapy: Therapy): Observable<Therapy> {
        const copy = this.convert(therapy);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(therapy: Therapy): Observable<Therapy> {
        const copy = this.convert(therapy);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Therapy> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(therapy: Therapy): Therapy {
        const copy: Therapy = Object.assign({}, therapy);
        return copy;
    }
}
