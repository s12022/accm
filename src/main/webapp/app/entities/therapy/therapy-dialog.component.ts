import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Therapy } from './therapy.model';
import { TherapyPopupService } from './therapy-popup.service';
import { TherapyService } from './therapy.service';
import { Company, CompanyService } from '../company';
import { Consumer, ConsumerService } from '../consumer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-therapy-dialog',
    templateUrl: './therapy-dialog.component.html'
})
export class TherapyDialogComponent implements OnInit {

    therapy: Therapy;
    isSaving: boolean;

    companies: Company[];

    consumers: Consumer[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private therapyService: TherapyService,
        private companyService: CompanyService,
        private consumerService: ConsumerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: ResponseWrapper) => { this.companies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.consumerService.query()
            .subscribe((res: ResponseWrapper) => { this.consumers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.therapy.id !== undefined) {
            this.subscribeToSaveResponse(
                this.therapyService.update(this.therapy));
        } else {
            this.subscribeToSaveResponse(
                this.therapyService.create(this.therapy));
        }
    }

    private subscribeToSaveResponse(result: Observable<Therapy>) {
        result.subscribe((res: Therapy) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Therapy) {
        this.eventManager.broadcast({ name: 'therapyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    trackConsumerById(index: number, item: Consumer) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-therapy-popup',
    template: ''
})
export class TherapyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private therapyPopupService: TherapyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.therapyPopupService
                    .open(TherapyDialogComponent as Component, params['id']);
            } else {
                this.therapyPopupService
                    .open(TherapyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
