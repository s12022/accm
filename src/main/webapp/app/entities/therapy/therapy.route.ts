import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TherapyComponent } from './therapy.component';
import { TherapyDetailComponent } from './therapy-detail.component';
import { TherapyPopupComponent } from './therapy-dialog.component';
import { TherapyDeletePopupComponent } from './therapy-delete-dialog.component';

@Injectable()
export class TherapyResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const therapyRoute: Routes = [
    {
        path: 'therapy',
        component: TherapyComponent,
        resolve: {
            'pagingParams': TherapyResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.therapy.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'therapy/:id',
        component: TherapyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.therapy.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const therapyPopupRoute: Routes = [
    {
        path: 'therapy-new',
        component: TherapyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.therapy.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'therapy/:id/edit',
        component: TherapyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.therapy.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'therapy/:id/delete',
        component: TherapyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.therapy.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
