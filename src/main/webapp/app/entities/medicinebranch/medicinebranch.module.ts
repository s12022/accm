import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccmSharedModule } from '../../shared';
import {
    MedicinebranchService,
    MedicinebranchPopupService,
    MedicinebranchComponent,
    MedicinebranchDetailComponent,
    MedicinebranchDialogComponent,
    MedicinebranchPopupComponent,
    MedicinebranchDeletePopupComponent,
    MedicinebranchDeleteDialogComponent,
    medicinebranchRoute,
    medicinebranchPopupRoute,
    MedicinebranchResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...medicinebranchRoute,
    ...medicinebranchPopupRoute,
];

@NgModule({
    imports: [
        AccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MedicinebranchComponent,
        MedicinebranchDetailComponent,
        MedicinebranchDialogComponent,
        MedicinebranchDeleteDialogComponent,
        MedicinebranchPopupComponent,
        MedicinebranchDeletePopupComponent,
    ],
    entryComponents: [
        MedicinebranchComponent,
        MedicinebranchDialogComponent,
        MedicinebranchPopupComponent,
        MedicinebranchDeleteDialogComponent,
        MedicinebranchDeletePopupComponent,
    ],
    providers: [
        MedicinebranchService,
        MedicinebranchPopupService,
        MedicinebranchResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmMedicinebranchModule {}
