import { BaseEntity } from './../../shared';

export class Medicinebranch implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public companies?: BaseEntity[],
        public consumers?: BaseEntity[],
    ) {
    }
}
