import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Medicinebranch } from './medicinebranch.model';
import { MedicinebranchService } from './medicinebranch.service';

@Component({
    selector: 'jhi-medicinebranch-detail',
    templateUrl: './medicinebranch-detail.component.html'
})
export class MedicinebranchDetailComponent implements OnInit, OnDestroy {

    medicinebranch: Medicinebranch;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private medicinebranchService: MedicinebranchService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMedicinebranches();
    }

    load(id) {
        this.medicinebranchService.find(id).subscribe((medicinebranch) => {
            this.medicinebranch = medicinebranch;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMedicinebranches() {
        this.eventSubscriber = this.eventManager.subscribe(
            'medicinebranchListModification',
            (response) => this.load(this.medicinebranch.id)
        );
    }
}
