import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Medicinebranch } from './medicinebranch.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MedicinebranchService {

    private resourceUrl = 'api/medicinebranches';
    private resourceSearchUrl = 'api/_search/medicinebranches';

    constructor(private http: Http) { }

    create(medicinebranch: Medicinebranch): Observable<Medicinebranch> {
        const copy = this.convert(medicinebranch);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(medicinebranch: Medicinebranch): Observable<Medicinebranch> {
        const copy = this.convert(medicinebranch);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Medicinebranch> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(medicinebranch: Medicinebranch): Medicinebranch {
        const copy: Medicinebranch = Object.assign({}, medicinebranch);
        return copy;
    }
}
