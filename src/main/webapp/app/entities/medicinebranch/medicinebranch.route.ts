import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MedicinebranchComponent } from './medicinebranch.component';
import { MedicinebranchDetailComponent } from './medicinebranch-detail.component';
import { MedicinebranchPopupComponent } from './medicinebranch-dialog.component';
import { MedicinebranchDeletePopupComponent } from './medicinebranch-delete-dialog.component';

@Injectable()
export class MedicinebranchResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const medicinebranchRoute: Routes = [
    {
        path: 'medicinebranch',
        component: MedicinebranchComponent,
        resolve: {
            'pagingParams': MedicinebranchResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.medicinebranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'medicinebranch/:id',
        component: MedicinebranchDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.medicinebranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const medicinebranchPopupRoute: Routes = [
    {
        path: 'medicinebranch-new',
        component: MedicinebranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.medicinebranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'medicinebranch/:id/edit',
        component: MedicinebranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.medicinebranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'medicinebranch/:id/delete',
        component: MedicinebranchDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.medicinebranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
