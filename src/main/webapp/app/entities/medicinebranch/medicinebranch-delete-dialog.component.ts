import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Medicinebranch } from './medicinebranch.model';
import { MedicinebranchPopupService } from './medicinebranch-popup.service';
import { MedicinebranchService } from './medicinebranch.service';

@Component({
    selector: 'jhi-medicinebranch-delete-dialog',
    templateUrl: './medicinebranch-delete-dialog.component.html'
})
export class MedicinebranchDeleteDialogComponent {

    medicinebranch: Medicinebranch;

    constructor(
        private medicinebranchService: MedicinebranchService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.medicinebranchService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'medicinebranchListModification',
                content: 'Deleted an medicinebranch'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-medicinebranch-delete-popup',
    template: ''
})
export class MedicinebranchDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private medicinebranchPopupService: MedicinebranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.medicinebranchPopupService
                .open(MedicinebranchDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
