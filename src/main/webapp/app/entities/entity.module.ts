import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AccmConsumerModule } from './consumer/consumer.module';
import { AccmChildModule } from './child/child.module';
import { AccmAddressModule } from './address/address.module';
import { AccmProductModule } from './product/product.module';
import { AccmDiseaseModule } from './disease/disease.module';
import { AccmTherapyModule } from './therapy/therapy.module';
import { AccmMedicinebranchModule } from './medicinebranch/medicinebranch.module';
import { AccmDatasourceModule } from './datasource/datasource.module';
import { AccmActiontypeModule } from './actiontype/actiontype.module';
import { AccmDivisionModule } from './division/division.module';
import { AccmCompanyModule } from './company/company.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        AccmConsumerModule,
        AccmChildModule,
        AccmAddressModule,
        AccmProductModule,
        AccmDiseaseModule,
        AccmTherapyModule,
        AccmMedicinebranchModule,
        AccmDatasourceModule,
        AccmActiontypeModule,
        AccmDivisionModule,
        AccmCompanyModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmEntityModule {}
