import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Consumer } from './consumer.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ConsumerService {

    private resourceUrl = 'api/consumers';
    private resourceSearchUrl = 'api/_search/consumers';

    constructor(private http: Http) { }

    create(consumer: Consumer): Observable<Consumer> {
        const copy = this.convert(consumer);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(consumer: Consumer): Observable<Consumer> {
        const copy = this.convert(consumer);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Consumer> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(consumer: Consumer): Consumer {
        const copy: Consumer = Object.assign({}, consumer);
        return copy;
    }
}
