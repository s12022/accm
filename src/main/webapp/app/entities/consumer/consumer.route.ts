import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ConsumerComponent } from './consumer.component';
import { ConsumerDetailComponent } from './consumer-detail.component';
import { ConsumerPopupComponent } from './consumer-dialog.component';
import { ConsumerDeletePopupComponent } from './consumer-delete-dialog.component';

@Injectable()
export class ConsumerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const consumerRoute: Routes = [
    {
        path: 'consumer',
        component: ConsumerComponent,
        resolve: {
            'pagingParams': ConsumerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'consumer/:id',
        component: ConsumerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const consumerPopupRoute: Routes = [
    {
        path: 'consumer-new',
        component: ConsumerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'consumer/:id/edit',
        component: ConsumerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'consumer/:id/delete',
        component: ConsumerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
