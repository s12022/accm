import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Consumer } from './consumer.model';
import { ConsumerPopupService } from './consumer-popup.service';
import { ConsumerService } from './consumer.service';
import { Address, AddressService } from '../address';
import { Product, ProductService } from '../product';
import { Therapy, TherapyService } from '../therapy';
import { Disease, DiseaseService } from '../disease';
import { Medicinebranch, MedicinebranchService } from '../medicinebranch';
import { Child, ChildService } from '../child';
import { Datasource, DatasourceService } from '../datasource';
import { ResponseWrapper } from '../../shared';
import { Subscription } from 'rxjs/Rx';
import {isNull, isUndefined} from 'util';

@Component({
    selector: 'jhi-consumer-dialog',
    templateUrl: './consumer-dialog.component.html'
})
export class ConsumerDialogComponent implements OnInit {

    consumer: Consumer;
    isSaving: boolean;

    addresses: Address[];

    products: Product[];

    therapies: Therapy[];

    diseases: Disease[];

    medicinebranches: Medicinebranch[];

    children: Child[];

    datasources: Datasource[];

    childEventSubscriber: Subscription;
    addressEventSubscriber: Subscription;
    datasourceEventSubscriber: Subscription;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private consumerService: ConsumerService,
        private addressService: AddressService,
        private productService: ProductService,
        private therapyService: TherapyService,
        private diseaseService: DiseaseService,
        private medicinebranchService: MedicinebranchService,
        private childService: ChildService,
        private datasourceService: DatasourceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.addressService.query()
            .subscribe((res: ResponseWrapper) => { this.addresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.therapyService.query()
            .subscribe((res: ResponseWrapper) => { this.therapies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.diseaseService.query()
            .subscribe((res: ResponseWrapper) => { this.diseases = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.medicinebranchService.query()
            .subscribe((res: ResponseWrapper) => { this.medicinebranches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.childService.query()
            .subscribe((res: ResponseWrapper) => { this.children = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.datasourceService.query()
            .subscribe((res: ResponseWrapper) => { this.datasources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.childEventSubscriber = this.eventManager
            .subscribe('childListModification', (response) => this.loadChildren());
        this.addressEventSubscriber = this.eventManager
            .subscribe('addressListModification', (response) => this.loadAddresses());
        this.datasourceEventSubscriber =
            this.eventManager.subscribe('datasourceListModification', (response) => this.loadDatasources());

    }

    public testChild(child: Child) {
        return child.parentCount < 2;
    }

    public getFreeChildren(children: Child[] ) {
        console.log('getFreeChildren');
        if (isUndefined(children) || isNull(children)) {
            console.log('undefined chldren                     nnn');
            return children;
        }
        return children
            .filter((child: Child) => {
                this.isParentCountFull(child);
            })
    }

    public getFreeChildrenConcat() {
            return this.children
                .filter((child: Child) => {
                    this.isParentCountFull(child);
                })
                .concat(this.consumer.children)
        }

    private isParentCountFull(child: Child) {
        console.log(child.parentCount);
        return child.parentCount < 2
    }

    loadChildren() {
        this.childService.query()
            .subscribe((res: ResponseWrapper) => {
                    this.children = res.json;
                },
                (res: ResponseWrapper) => this.onError(res.json));
    }

    loadAddresses() {
        this.addressService.query()
            .subscribe((res: ResponseWrapper) => { this.addresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadDatasources() {
        this.datasourceService.query()
            .subscribe((res: ResponseWrapper) => { this.datasources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.consumer.id !== undefined) {
            this.subscribeToSaveResponse(
                this.consumerService.update(this.consumer));
        } else {
            this.subscribeToSaveResponse(
                this.consumerService.create(this.consumer));
        }
    }

    private subscribeToSaveResponse(result: Observable<Consumer>) {
        result.subscribe((res: Consumer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Consumer) {
        this.eventManager.broadcast({ name: 'consumerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackAddressById(index: number, item: Address) {
        return item.id;
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }

    trackTherapyById(index: number, item: Therapy) {
        return item.id;
    }

    trackDiseaseById(index: number, item: Disease) {
        return item.id;
    }

    trackMedicinebranchById(index: number, item: Medicinebranch) {
        return item.id;
    }

    trackChildById(index: number, item: Child) {
        return item.id;
    }

    trackDatasourceById(index: number, item: Datasource) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-consumer-popup',
    template: ''
})
export class ConsumerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consumerPopupService: ConsumerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.consumerPopupService
                    .open(ConsumerDialogComponent as Component, params['id']);
            } else {
                this.consumerPopupService
                    .open(ConsumerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
