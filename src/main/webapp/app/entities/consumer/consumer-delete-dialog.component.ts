import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Consumer } from './consumer.model';
import { ConsumerPopupService } from './consumer-popup.service';
import { ConsumerService } from './consumer.service';

@Component({
    selector: 'jhi-consumer-delete-dialog',
    templateUrl: './consumer-delete-dialog.component.html'
})
export class ConsumerDeleteDialogComponent {

    consumer: Consumer;

    constructor(
        private consumerService: ConsumerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.consumerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'consumerListModification',
                content: 'Deleted an consumer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-consumer-delete-popup',
    template: ''
})
export class ConsumerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consumerPopupService: ConsumerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.consumerPopupService
                .open(ConsumerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
