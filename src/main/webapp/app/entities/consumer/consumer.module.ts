import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccmSharedModule } from '../../shared';
import {
    ConsumerService,
    ConsumerPopupService,
    ConsumerComponent,
    ConsumerDetailComponent,
    ConsumerDialogComponent,
    ConsumerPopupComponent,
    ConsumerDeletePopupComponent,
    ConsumerDeleteDialogComponent,
    consumerRoute,
    consumerPopupRoute,
    ConsumerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...consumerRoute,
    ...consumerPopupRoute,
];

@NgModule({
    imports: [
        AccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ConsumerComponent,
        ConsumerDetailComponent,
        ConsumerDialogComponent,
        ConsumerDeleteDialogComponent,
        ConsumerPopupComponent,
        ConsumerDeletePopupComponent,
    ],
    entryComponents: [
        ConsumerComponent,
        ConsumerDialogComponent,
        ConsumerPopupComponent,
        ConsumerDeleteDialogComponent,
        ConsumerDeletePopupComponent,
    ],
    providers: [
        ConsumerService,
        ConsumerPopupService,
        ConsumerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmConsumerModule {}
