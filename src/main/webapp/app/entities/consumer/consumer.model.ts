import { BaseEntity } from './../../shared';

const enum Gender {
    'MALE',
    'FEMALE',
    'OTHER'
}

const enum UserType {
    'CONSUMER',
    'SPECIALIST',
    'OTHER'
}

export class Consumer implements BaseEntity {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public gender?: Gender,
        public email?: string,
        public accepted?: boolean,
        public active?: boolean,
        public phoneNumber?: string,
        public userType?: UserType,
        public userComment?: string,
        public address?: BaseEntity,
        public products?: BaseEntity[],
        public therapies?: BaseEntity[],
        public diseases?: BaseEntity[],
        public branches?: BaseEntity[],
        public children?: BaseEntity[],
        public datasources?: BaseEntity[],
    ) {
        this.accepted = false;
        this.active = false;
    }
}
