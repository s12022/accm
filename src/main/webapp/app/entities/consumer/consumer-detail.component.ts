import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Consumer } from './consumer.model';
import { ConsumerService } from './consumer.service';

@Component({
    selector: 'jhi-consumer-detail',
    templateUrl: './consumer-detail.component.html'
})
export class ConsumerDetailComponent implements OnInit, OnDestroy {

    consumer: Consumer;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private consumerService: ConsumerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConsumers();
    }

    load(id) {
        this.consumerService.find(id).subscribe((consumer) => {
            this.consumer = consumer;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConsumers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'consumerListModification',
            (response) => this.load(this.consumer.id)
        );
    }
}
