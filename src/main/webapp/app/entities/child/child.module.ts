import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccmSharedModule } from '../../shared';
import {
    ChildService,
    ChildPopupService,
    ChildComponent,
    ChildDetailComponent,
    ChildDialogComponent,
    ChildPopupComponent,
    ChildDeletePopupComponent,
    ChildDeleteDialogComponent,
    childRoute,
    childPopupRoute,
    ChildResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...childRoute,
    ...childPopupRoute,
];

@NgModule({
    imports: [
        AccmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ChildComponent,
        ChildDetailComponent,
        ChildDialogComponent,
        ChildDeleteDialogComponent,
        ChildPopupComponent,
        ChildDeletePopupComponent,
    ],
    entryComponents: [
        ChildComponent,
        ChildDialogComponent,
        ChildPopupComponent,
        ChildDeleteDialogComponent,
        ChildDeletePopupComponent,
    ],
    providers: [
        ChildService,
        ChildPopupService,
        ChildResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccmChildModule {}
