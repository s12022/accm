import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Child } from './child.model';
import { ChildPopupService } from './child-popup.service';
import { ChildService } from './child.service';
import { Consumer, ConsumerService } from '../consumer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-child-dialog',
    templateUrl: './child-dialog.component.html'
})
export class ChildDialogComponent implements OnInit {

    child: Child;
    isSaving: boolean;

    consumers: Consumer[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private childService: ChildService,
        private consumerService: ConsumerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.consumerService.query()
            .subscribe((res: ResponseWrapper) => { this.consumers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.child.id !== undefined) {
            this.subscribeToSaveResponse(
                this.childService.update(this.child));
        } else {
            this.subscribeToSaveResponse(
                this.childService.create(this.child));
        }
    }

    private subscribeToSaveResponse(result: Observable<Child>) {
        result.subscribe((res: Child) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Child) {
        this.eventManager.broadcast({ name: 'childListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackConsumerById(index: number, item: Consumer) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-child-popup',
    template: ''
})
export class ChildPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private childPopupService: ChildPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.childPopupService
                    .open(ChildDialogComponent as Component, params['id']);
            } else {
                this.childPopupService
                    .open(ChildDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
