import { BaseEntity } from './../../shared';

export class Child implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public birthdate?: any,
        public parentCount?: number,
        public consumers?: BaseEntity[],
    ) {
    }
}
