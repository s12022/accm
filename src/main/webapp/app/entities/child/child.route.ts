import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ChildComponent } from './child.component';
import { ChildDetailComponent } from './child-detail.component';
import { ChildPopupComponent } from './child-dialog.component';
import { ChildDeletePopupComponent } from './child-delete-dialog.component';

@Injectable()
export class ChildResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const childRoute: Routes = [
    {
        path: 'child',
        component: ChildComponent,
        resolve: {
            'pagingParams': ChildResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.child.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'child/:id',
        component: ChildDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.child.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const childPopupRoute: Routes = [
    {
        path: 'child-new',
        component: ChildPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.child.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'child-new-from-consumer',
        component: ChildPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.consumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'child/:id/edit',
        component: ChildPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.child.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'child/:id/delete',
        component: ChildDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'accmApp.child.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
