import { BaseEntity } from './../../shared';

export class Company implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public divisions?: BaseEntity[],
        public products?: BaseEntity[],
        public therapies?: BaseEntity[],
        public diseases?: BaseEntity[],
        public branches?: BaseEntity[],
    ) {
    }
}
