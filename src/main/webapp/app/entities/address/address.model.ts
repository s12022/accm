import { BaseEntity } from './../../shared';

export class Address implements BaseEntity {
    constructor(
        public id?: number,
        public provinceName?: string,
        public cityName?: string,
        public streetName?: string,
    ) {
    }
}
