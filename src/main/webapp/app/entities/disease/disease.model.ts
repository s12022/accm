import { BaseEntity } from './../../shared';

export class Disease implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public companies?: BaseEntity[],
        public consumers?: BaseEntity[],
    ) {
    }
}
