import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Disease } from './disease.model';
import { DiseasePopupService } from './disease-popup.service';
import { DiseaseService } from './disease.service';
import { Company, CompanyService } from '../company';
import { Consumer, ConsumerService } from '../consumer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-disease-dialog',
    templateUrl: './disease-dialog.component.html'
})
export class DiseaseDialogComponent implements OnInit {

    disease: Disease;
    isSaving: boolean;

    companies: Company[];

    consumers: Consumer[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private diseaseService: DiseaseService,
        private companyService: CompanyService,
        private consumerService: ConsumerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: ResponseWrapper) => { this.companies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.consumerService.query()
            .subscribe((res: ResponseWrapper) => { this.consumers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.disease.id !== undefined) {
            this.subscribeToSaveResponse(
                this.diseaseService.update(this.disease));
        } else {
            this.subscribeToSaveResponse(
                this.diseaseService.create(this.disease));
        }
    }

    private subscribeToSaveResponse(result: Observable<Disease>) {
        result.subscribe((res: Disease) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Disease) {
        this.eventManager.broadcast({ name: 'diseaseListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    trackConsumerById(index: number, item: Consumer) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-disease-popup',
    template: ''
})
export class DiseasePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private diseasePopupService: DiseasePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.diseasePopupService
                    .open(DiseaseDialogComponent as Component, params['id']);
            } else {
                this.diseasePopupService
                    .open(DiseaseDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
