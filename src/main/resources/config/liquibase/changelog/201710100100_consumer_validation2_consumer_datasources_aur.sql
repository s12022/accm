--liquibase formatted sql
--changeset skynet:consumer_validation2_consumer_datasources_aur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DATASOURCES_AUR
AFTER UPDATE
    ON CONSUMER_DATASOURCES
FOR EACH ROW
    DECLARE
        l_datasources_count_old CONSUMER.DATASOURCES_COUNT%TYPE;
        l_datasources_count_new CONSUMER.DATASOURCES_COUNT%TYPE;
    BEGIN
        --
        IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
        THEN
            --
            SELECT DATASOURCES_COUNT
            INTO l_datasources_count_old
            FROM CONSUMER
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            IF l_datasources_count_old > 0
            THEN
                l_datasources_count_old := l_datasources_count_old - 1;
            END IF;
            --
            --
            UPDATE CONSUMER
            SET DATASOURCES_COUNT = l_datasources_count_old
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            --
            SELECT DATASOURCES_COUNT
            INTO l_datasources_count_new
            FROM CONSUMER
            WHERE ID = :NEW.CONSUMERS_ID;
            l_datasources_count_new := l_datasources_count_new + 1;
            --
            UPDATE CONSUMER
            SET DATASOURCES_COUNT = l_datasources_count_new
            WHERE ID = :NEW.CONSUMERS_ID;
        END IF;
    END;
