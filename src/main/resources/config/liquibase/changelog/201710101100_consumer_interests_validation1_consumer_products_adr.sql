--liquibase formatted sql
--changeset skynet:consumer_interests_validation1_consumer_products_adr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_PRODUCTS_ADR
AFTER DELETE
    ON CONSUMER_PRODUCTS
FOR EACH ROW
    DECLARE
        l_products_count CONSUMER.PRODUCTS_COUNT%TYPE;
    BEGIN
        SELECT PRODUCTS_COUNT
        INTO l_products_count
        FROM CONSUMER
        WHERE ID = :OLD.CONSUMERS_ID;
        --
        IF l_products_count > 0
        THEN
            l_products_count := l_products_count - 1;
            UPDATE CONSUMER
            SET PRODUCTS_COUNT = l_products_count
            WHERE ID = :OLD.CONSUMERS_ID;
        END IF;
    END;
