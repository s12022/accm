--liquibase formatted sql
--changeset skynet:consumer_invisible_columns_comments endDelimiter:; dbms:oracle stripComments:false
COMMENT ON COLUMN CONSUMER.PRODUCTS_COUNT IS 'Ukryta kolumna przechowująca ilość produktów, którymi zainteresowany jest klinet';
COMMENT ON COLUMN CONSUMER.THERAPIES_COUNT IS 'Ukryta kolumna przechowująca ilość terapii, którymi zainteresowany jest klinet';
COMMENT ON COLUMN CONSUMER.BRANCHES_COUNT IS 'Ukryta kolumna przechowująca ilość gałęzi medycyny, którymi zainteresowany jest klinet';
COMMENT ON COLUMN CONSUMER.DISEASES_COUNT IS 'Ukryta kolumna przechowująca ilość schorzeń, którymi zainteresowany jest klinet';
COMMENT ON COLUMN CONSUMER.DATASOURCES_COUNT IS 'Ukryta kolumna przechowująca ilość źródeł danych, w których wystąpił klient';
--rollback COMMENT ON COLUMN CONSUMER.PRODUCTS_COUNT IS '';
--rollback COMMENT ON COLUMN CONSUMER.THERAPIES_COUNT IS '';
--rollback COMMENT ON COLUMN CONSUMER.BRANCHES_COUNT IS '';
--rollback COMMENT ON COLUMN CONSUMER.DISEASES_COUNT IS '';
--rollback COMMENT ON COLUMN CONSUMER.DATASOURCES_COUNT IS '';
