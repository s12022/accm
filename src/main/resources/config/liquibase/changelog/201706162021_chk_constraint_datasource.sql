--liquibase formatted sql
--changeset skynet:constraint_datasource_DATASOURCE_TURN_CHK dbms:oracle
ALTER TABLE DATASOURCE ADD CONSTRAINT DATASOURCE_TURN_CHK CHECK (TURN > 0);
