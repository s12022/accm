--liquibase formatted sql
--changeset skynet:consumer_interests_validation7_8_9_consumer_branches_drop_unnessesary_triggers_air endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_BRANCHES_AIR
--changeset skynet:consumer_interests_validation7_8_9_consumer_branches_drop_unnessesary_triggers_adr endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_BRANCHES_ADR
--changeset skynet:consumer_interests_validation7_8_9_consumer_branches_drop_unnessesary_triggers_aur endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_BRANCHES_AUR
--changeset skynet:consumer_interests_validation7_8_9_consumer_branches_aidur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_BRANCHES_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_BRANCHES
FOR EACH ROW
    --
    DECLARE
        l_branches_count CONSUMER.BRANCHES_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT BRANCHES_COUNT
                INTO l_branches_count
                FROM CONSUMER
                WHERE ID = :NEW.CONSUMERS_ID;
                --
                l_branches_count := l_branches_count + 1;
                --
                UPDATE CONSUMER
                SET BRANCHES_COUNT = l_branches_count
                WHERE ID = :NEW.CONSUMERS_ID;
            END;
            --
            --
        ELSIF DELETING
            THEN
                --
                BEGIN
                    SELECT BRANCHES_COUNT
                    INTO l_branches_count
                    FROM CONSUMER
                    WHERE ID = :OLD.CONSUMERS_ID;
                    --
                    IF l_branches_count > 0
                    THEN
                        l_branches_count := l_branches_count - 1;
                        UPDATE CONSUMER
                        SET BRANCHES_COUNT = l_branches_count
                        WHERE ID = :OLD.CONSUMERS_ID;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                --
                DECLARE
                    l_branches_count_old CONSUMER.BRANCHES_COUNT%TYPE;
                    l_branches_count_new CONSUMER.BRANCHES_COUNT%TYPE;
                BEGIN
                    --
                    IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
                    THEN
                        --
                        SELECT BRANCHES_COUNT
                        INTO l_branches_count_old
                        FROM CONSUMER
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        IF l_branches_count_old > 0
                        THEN
                            l_branches_count_old := l_branches_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CONSUMER
                        SET BRANCHES_COUNT = l_branches_count_old
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        --
                        SELECT BRANCHES_COUNT
                        INTO l_branches_count_new
                        FROM CONSUMER
                        WHERE ID = :NEW.CONSUMERS_ID;
                        l_branches_count_new := l_branches_count_new + 1;
                        --
                        UPDATE CONSUMER
                        SET BRANCHES_COUNT = l_branches_count_new
                        WHERE ID = :NEW.CONSUMERS_ID;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
