--liquibase formatted sql
--changeset skynet:consumer_validation1_consumer_datasources_drop_unnessesary_triggers_CONSUMER_CHILD_ADR endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_CHILD_ADR
--changeset skynet:consumer_validation1_consumer_datasources_drop_unnessesary_triggers_CONSUMER_CHILD_AIR endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_CHILD_AIR
--changeset skynet:consumer_validation1_consumer_datasources_drop_unnessesary_triggers_CONSUMER_CHILD_AUR endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_CHILD_AUR
--changeset skynet:consumer_validation1_consumer_children_aidur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_CHILDREN_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_CHILDREN
FOR EACH ROW
    --
    DECLARE
        l_parent_count CHILD.PARENT_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT nvl(PARENT_COUNT, 0)
                INTO l_parent_count
                FROM CHILD
                WHERE ID = :NEW.children_id;
                --
                l_parent_count := l_parent_count + 1;
                --
                UPDATE CHILD
                SET PARENT_COUNT = l_parent_count
                WHERE ID = :NEW.children_id;
            END;
            --
            --
        ELSIF DELETING
            THEN
                BEGIN
                    SELECT PARENT_COUNT
                    INTO l_parent_count
                    FROM CHILD
                    WHERE ID = :OLD.children_id;
                    IF l_parent_count > 0
                    THEN
                        l_parent_count := l_parent_count - 1;
                        UPDATE CHILD
                        SET PARENT_COUNT = l_parent_count
                        WHERE ID = :OLD.children_id;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                DECLARE
                    l_parent_count_old CHILD.PARENT_COUNT%TYPE;
                    l_parent_count_new CHILD.PARENT_COUNT%TYPE;
                BEGIN
                    --
                    IF :OLD.children_id != :NEW.children_id
                    THEN
                        --
                        SELECT PARENT_COUNT
                        INTO l_parent_count_old
                        FROM CHILD
                        WHERE ID = :OLD.children_id;
                        --
                        IF l_parent_count_old > 0
                        THEN
                            l_parent_count_old := l_parent_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CHILD
                        SET PARENT_COUNT = l_parent_count_old
                        WHERE ID = :OLD.children_id;
                        --
                        --
                        SELECT PARENT_COUNT
                        INTO l_parent_count_new
                        FROM CHILD
                        WHERE ID = :NEW.children_id;
                        l_parent_count_new := l_parent_count_new + 1;
                        --
                        UPDATE CHILD
                        SET PARENT_COUNT = l_parent_count_new
                        WHERE ID = :NEW.children_id;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
