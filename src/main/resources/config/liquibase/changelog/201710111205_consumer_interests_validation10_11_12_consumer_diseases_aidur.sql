--liquibase formatted sql
--changeset skynet:consumer_interests_validation10_11_12_consumer_diseases_drop_unnessesary_triggers_air endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_DISEASES_AIR
--changeset skynet:consumer_interests_validation10_11_12_consumer_diseases_drop_unnessesary_triggers_adr endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_DISEASES_ADR
--changeset skynet:consumer_interests_validation10_11_12_consumer_diseases_drop_unnessesary_triggers_aur endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_DISEASES_AUR
--changeset skynet:consumer_interests_validation10_11_12_consumer_diseases_aidur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DISEASES_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_DISEASES
FOR EACH ROW
    --
    DECLARE
        l_diseases_count CONSUMER.DISEASES_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT DISEASES_COUNT
                INTO l_diseases_count
                FROM CONSUMER
                WHERE ID = :NEW.CONSUMERS_ID;
                --
                l_diseases_count := l_diseases_count + 1;
                --
                UPDATE CONSUMER
                SET DISEASES_COUNT = l_diseases_count
                WHERE ID = :NEW.CONSUMERS_ID;
            END;
            --
            --
        ELSIF DELETING
            THEN
                --
                BEGIN
                    SELECT DISEASES_COUNT
                    INTO l_diseases_count
                    FROM CONSUMER
                    WHERE ID = :OLD.CONSUMERS_ID;
                    --
                    IF l_diseases_count > 0
                    THEN
                        l_diseases_count := l_diseases_count - 1;
                        UPDATE CONSUMER
                        SET DISEASES_COUNT = l_diseases_count
                        WHERE ID = :OLD.CONSUMERS_ID;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                --
                BEGIN
                    --
                    IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
                    THEN
                        --
                        SELECT DISEASES_COUNT
                        INTO l_diseases_count_old
                        FROM CONSUMER
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        IF l_diseases_count_old > 0
                        THEN
                            l_diseases_count_old := l_diseases_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CONSUMER
                        SET DISEASES_COUNT = l_diseases_count_old
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        --
                        SELECT DISEASES_COUNT
                        INTO l_diseases_count_new
                        FROM CONSUMER
                        WHERE ID = :NEW.CONSUMERS_ID;
                        l_diseases_count_new := l_diseases_count_new + 1;
                        --
                        UPDATE CONSUMER
                        SET DISEASES_COUNT = l_diseases_count_new
                        WHERE ID = :NEW.CONSUMERS_ID;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
