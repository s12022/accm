--liquibase formatted sql
--changeset skynet:compile_invalid_triggers dbms:oracle endDelimiter:\n; runOnChange:true
BEGIN FOR cur IN (SELECT
                      OBJECT_NAME,
                      OBJECT_TYPE,
                      owner
                  FROM all_objects
                  WHERE object_type IN ('TRIGGER') AND status = 'INVALID' ) LOOP
    BEGIN
        EXECUTE IMMEDIATE 'alter ' || cur.OBJECT_TYPE || ' "' || cur.owner || '"."' || cur.OBJECT_NAME || '" compile';
        EXCEPTION
        WHEN OTHERS THEN NULL;
    END;
END LOOP;
END;
