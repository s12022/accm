--liquibase formatted sql
--changeset skynet:consumer_validation1_consumer_datasources_adr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DATASOURCES_ADR
AFTER DELETE
    ON CONSUMER_DATASOURCES
FOR EACH ROW
    DECLARE
        l_datasources_count CONSUMER.DATASOURCES_COUNT%TYPE;
    BEGIN
        SELECT DATASOURCES_COUNT
        INTO l_datasources_count
        FROM CONSUMER
        WHERE ID = :OLD.CONSUMERS_ID;

        IF l_datasources_count > 0
        THEN
            l_datasources_count := l_datasources_count - 1;
            UPDATE CONSUMER
            SET DATASOURCES_COUNT = l_datasources_count
            WHERE ID = :OLD.CONSUMERS_ID;
        END IF;
    END;
