--liquibase formatted sql
--changeset skynet:consumer_interests_validation7_consumer_branches_adr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_BRANCHES_ADR
AFTER DELETE
    ON CONSUMER_BRANCHES
FOR EACH ROW
    DECLARE
        l_branches_count CONSUMER.BRANCHES_COUNT%TYPE;
    BEGIN
        SELECT BRANCHES_COUNT
        INTO l_branches_count
        FROM CONSUMER
        WHERE ID = :OLD.CONSUMERS_ID;
        --
        IF l_branches_count > 0
        THEN
            l_branches_count := l_branches_count - 1;
            UPDATE CONSUMER
            SET BRANCHES_COUNT = l_branches_count
            WHERE ID = :OLD.CONSUMERS_ID;
        END IF;
    END;
