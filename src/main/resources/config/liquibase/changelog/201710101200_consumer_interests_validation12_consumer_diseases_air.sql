--liquibase formatted sql
--changeset skynet:consumer_interests_validation12_consumer_diseases_air splitStatements:false dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DISEASES_AIR
AFTER INSERT
    ON CONSUMER_DISEASES
FOR EACH ROW
    DECLARE
        l_diseases_count CONSUMER.DISEASES_COUNT%TYPE;
    BEGIN
        SELECT DISEASES_COUNT
        INTO l_diseases_count
        FROM CONSUMER
        WHERE ID = :NEW.CONSUMERS_ID;
        --
        l_diseases_count := l_diseases_count + 1;
        --
        UPDATE CONSUMER
        SET DISEASES_COUNT = l_diseases_count
        WHERE ID = :NEW.CONSUMERS_ID;
    END;
