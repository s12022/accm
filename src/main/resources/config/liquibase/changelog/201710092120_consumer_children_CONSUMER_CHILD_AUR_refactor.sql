--liquibase formatted sql
--changeset skynet:consumer_children_validation3_CONSUMER_CHILD_AUR endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_CHILD_AUR
AFTER UPDATE
    ON CONSUMER_CHILDREN
FOR EACH ROW
    DECLARE
        l_parent_count_old CHILD.PARENT_COUNT%TYPE;
        l_parent_count_new CHILD.PARENT_COUNT%TYPE;
    BEGIN
        --
        IF :OLD.children_id != :NEW.children_id
        THEN
            --
            SELECT PARENT_COUNT
            INTO l_parent_count_old
            FROM CHILD
            WHERE ID = :OLD.children_id;
            --
            IF l_parent_count_old > 0
            THEN
                l_parent_count_old := l_parent_count_old - 1;
            END IF;
            --
            --
            UPDATE CHILD
            SET PARENT_COUNT = l_parent_count_old
            WHERE ID = :OLD.children_id;
            --
            --
            SELECT PARENT_COUNT
            INTO l_parent_count_new
            FROM CHILD
            WHERE ID = :NEW.children_id;
            l_parent_count_new := l_parent_count_new + 1;
            --
            UPDATE CHILD
            SET PARENT_COUNT = l_parent_count_new
            WHERE ID = :NEW.children_id;
        END IF;
    END;
