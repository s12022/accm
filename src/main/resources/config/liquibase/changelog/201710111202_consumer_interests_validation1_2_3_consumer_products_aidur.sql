--liquibase formatted sql
--changeset skynet:consumer_interests_validation1_2_3_consumer_products_drop_unnessesary_triggers_adr endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_PRODUCTS_ADR
--changeset skynet:consumer_interests_validation1_2_3_consumer_products_drop_unnessesary_triggers_air endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_PRODUCTS_AIR
--changeset skynet:consumer_interests_validation1_2_3_consumer_products_drop_unnessesary_triggers_aur endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_PRODUCTS_AUR
--changeset skynet:consumer_interests_validation1_2_3_consumer_products_aiudr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_PRODUCTS_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_PRODUCTS
FOR EACH ROW
    --
    DECLARE
        l_products_count CONSUMER.PRODUCTS_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT PRODUCTS_COUNT
                INTO l_products_count
                FROM CONSUMER
                WHERE ID = :NEW.CONSUMERS_ID;
                --
                l_products_count := l_products_count + 1;
                --
                UPDATE CONSUMER
                SET PRODUCTS_COUNT = l_products_count
                WHERE ID = :NEW.CONSUMERS_ID;
            END;
            --
            --
        ELSIF DELETING
            THEN
                --
                BEGIN
                    SELECT PRODUCTS_COUNT
                    INTO l_products_count
                    FROM CONSUMER
                    WHERE ID = :OLD.CONSUMERS_ID;
                    --
                    IF l_products_count > 0
                    THEN
                        l_products_count := l_products_count - 1;
                        UPDATE CONSUMER
                        SET PRODUCTS_COUNT = l_products_count
                        WHERE ID = :OLD.CONSUMERS_ID;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                DECLARE
                    l_products_count_old CONSUMER.PRODUCTS_COUNT%TYPE;
                    l_products_count_new CONSUMER.PRODUCTS_COUNT%TYPE;
                BEGIN
                    --
                    IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
                    THEN
                        --
                        SELECT PRODUCTS_COUNT
                        INTO l_products_count_old
                        FROM CONSUMER
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        IF l_products_count_old > 0
                        THEN
                            l_products_count_old := l_products_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CONSUMER
                        SET PRODUCTS_COUNT = l_products_count_old
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        --
                        SELECT PRODUCTS_COUNT
                        INTO l_products_count_new
                        FROM CONSUMER
                        WHERE ID = :NEW.CONSUMERS_ID;
                        l_products_count_new := l_products_count_new + 1;
                        --
                        UPDATE CONSUMER
                        SET PRODUCTS_COUNT = l_products_count_new
                        WHERE ID = :NEW.CONSUMERS_ID;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
