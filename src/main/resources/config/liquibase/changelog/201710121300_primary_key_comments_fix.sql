--liquibase formatted sql
--changeset skynet:count_relation_size_function endDelimiter:\n; dbms:oracle stripComments:false
--rollback drop function count_relation_size;
BEGIN
    FOR cc IN (
    SELECT 'COMMENT ON COLUMN '
           || table_name
           || '.'
           || COLUMN_NAME
           || ' IS ' AS l_sql_string
    FROM
        all_col_comments
    WHERE
        owner like 'ACCM%'
        AND
        comments IS NULL
        AND
        column_name = 'ID'
        AND
        table_name NOT LIKE ('DATABASECHANGELOG%')
        AND
        table_name NOT LIKE ('JHI%')
    ) LOOP
        BEGIN
            EXECUTE IMMEDIATE cc.l_sql_string || '''Kolumna sztucznego klucza głównego''';
        END;
    END LOOP;
END;
