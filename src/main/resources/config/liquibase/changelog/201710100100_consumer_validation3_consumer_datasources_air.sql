--liquibase formatted sql
--changeset skynet:consumer_validation3_consumer_datasources_air splitStatements:false dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DATASOURCES_AIR
AFTER INSERT
    ON CONSUMER_DATASOURCES
FOR EACH ROW
    DECLARE
        l_datasources_count CONSUMER.DATASOURCES_COUNT%TYPE;
    BEGIN
        SELECT nvl(DATASOURCES_COUNT, 0)
        INTO l_datasources_count
        FROM CONSUMER
        WHERE ID = :NEW.CONSUMERS_ID;
        --
        l_datasources_count := l_datasources_count + 1;
        --
        UPDATE CONSUMER
        SET DATASOURCES_COUNT = l_datasources_count
        WHERE ID = :NEW.CONSUMERS_ID;
    END;
