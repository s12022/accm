--liquibase formatted sql
--changeset skynet:consumer_interests_validation2_consumer_products_aur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_PRODUCTS_AUR
AFTER UPDATE
    ON CONSUMER_PRODUCTS
FOR EACH ROW
    DECLARE
        l_products_count_old CONSUMER.PRODUCTS_COUNT%TYPE;
        l_products_count_new CONSUMER.PRODUCTS_COUNT%TYPE;
    BEGIN
        --
        IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
        THEN
            --
            SELECT PRODUCTS_COUNT
            INTO l_products_count_old
            FROM CONSUMER
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            IF l_products_count_old > 0
            THEN
                l_products_count_old := l_products_count_old - 1;
            END IF;
            --
            --
            UPDATE CONSUMER
            SET PRODUCTS_COUNT = l_products_count_old
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            --
            SELECT PRODUCTS_COUNT
            INTO l_products_count_new
            FROM CONSUMER
            WHERE ID = :NEW.CONSUMERS_ID;
            l_products_count_new := l_products_count_new + 1;
            --
            UPDATE CONSUMER
            SET PRODUCTS_COUNT = l_products_count_new
            WHERE ID = :NEW.CONSUMERS_ID;
        END IF;
    END;
