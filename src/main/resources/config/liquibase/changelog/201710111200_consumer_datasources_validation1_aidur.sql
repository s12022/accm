--liquibase formatted sql
--changeset skynet:consumer_validation1_consumer_datasources_aidur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DATASOURCES_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_DATASOURCES
FOR EACH ROW
    --
    DECLARE
        l_datasources_count CONSUMER.DATASOURCES_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT nvl(DATASOURCES_COUNT, 0)
                INTO l_datasources_count
                FROM CONSUMER
                WHERE ID = :NEW.CONSUMERS_ID;
                --
                l_datasources_count := l_datasources_count + 1;
                --
                UPDATE CONSUMER
                SET DATASOURCES_COUNT = l_datasources_count
                WHERE id = :NEW.CONSUMERS_ID;
            END;
            --
            --
        ELSIF DELETING
            THEN
                BEGIN
                    SELECT DATASOURCES_COUNT
                    INTO l_datasources_count
                    FROM CONSUMER
                    WHERE ID = :OLD.CONSUMERS_ID;

                    IF l_datasources_count > 0
                    THEN
                        l_datasources_count := l_datasources_count - 1;
                        UPDATE CONSUMER
                        SET DATASOURCES_COUNT = l_datasources_count
                        WHERE ID = :OLD.CONSUMERS_ID;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                DECLARE
                    l_datasources_count_old CONSUMER.DATASOURCES_COUNT%TYPE;
                    l_datasources_count_new CONSUMER.DATASOURCES_COUNT%TYPE;
                BEGIN
                    --
                    IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
                    THEN
                        --
                        SELECT DATASOURCES_COUNT
                        INTO l_datasources_count_old
                        FROM CONSUMER
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        IF l_datasources_count_old > 0
                        THEN
                            l_datasources_count_old := l_datasources_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CONSUMER
                        SET DATASOURCES_COUNT = l_datasources_count_old
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        --
                        SELECT DATASOURCES_COUNT
                        INTO l_datasources_count_new
                        FROM CONSUMER
                        WHERE ID = :NEW.CONSUMERS_ID;
                        l_datasources_count_new := l_datasources_count_new + 1;
                        --
                        UPDATE CONSUMER
                        SET DATASOURCES_COUNT = l_datasources_count_new
                        WHERE ID = :NEW.CONSUMERS_ID;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
