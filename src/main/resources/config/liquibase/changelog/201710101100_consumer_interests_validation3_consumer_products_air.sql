--liquibase formatted sql
--changeset skynet:consumer_interests_validation3_consumer_products_AIR splitStatements:false dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_PRODUCTS_AIR
AFTER INSERT
    ON CONSUMER_PRODUCTS
FOR EACH ROW
    DECLARE
        l_products_count CONSUMER.PRODUCTS_COUNT%TYPE;
    BEGIN
        SELECT PRODUCTS_COUNT
        INTO l_products_count
        FROM CONSUMER
        WHERE ID = :NEW.CONSUMERS_ID;
        --
        l_products_count := l_products_count + 1;
        --
        UPDATE CONSUMER
        SET PRODUCTS_COUNT = l_products_count
        WHERE ID = :NEW.CONSUMERS_ID;
    END;
