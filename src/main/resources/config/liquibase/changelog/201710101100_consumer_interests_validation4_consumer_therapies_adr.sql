--liquibase formatted sql
--changeset skynet:consumer_interests_validation4_consumer_therapies_adr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_THERAPIES_ADR
AFTER DELETE
    ON CONSUMER_THERAPIES
FOR EACH ROW
    DECLARE
        l_therapies_count CONSUMER.THERAPIES_COUNT%TYPE;
    BEGIN
        SELECT THERAPIES_COUNT
        INTO l_therapies_count
        FROM CONSUMER
        WHERE ID = :OLD.CONSUMERS_ID;
        --
        IF l_therapies_count > 0
        THEN
            l_therapies_count := l_therapies_count - 1;
            UPDATE CONSUMER
            SET THERAPIES_COUNT = l_therapies_count
            WHERE ID = :OLD.CONSUMERS_ID;
        END IF;
    END;
