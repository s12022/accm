--liquibase formatted sql
--changeset skynet:consumer_children_validation4_CONSUMER_CHILD_ADR endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_CHILD_ADR
AFTER DELETE
    ON CONSUMER_CHILDREN
FOR EACH ROW
    DECLARE
        l_parent_count CHILD.PARENT_COUNT%TYPE;
    BEGIN
        SELECT PARENT_COUNT
        INTO l_parent_count
        FROM CHILD
        WHERE ID = :OLD.children_id;
        l_parent_count := l_parent_count - 1;
        --
        UPDATE CHILD
        SET PARENT_COUNT = l_parent_count
        WHERE ID = :OLD.children_id;
    END;
