--liquibase formatted sql
--changeset skynet:consumer_interests_validation13_check dbms:oracle
ALTER TABLE CONSUMER
    ADD CONSTRAINT CONSUMER_INTERESTS_CHK CHECK (
    PRODUCTS_COUNT > 0
    OR
    THERAPIES_COUNT > 0
    OR
    BRANCHES_COUNT > 0
    OR
    DISEASES_COUNT > 0
)
    INITIALLY DEFERRED DEFERRABLE;
