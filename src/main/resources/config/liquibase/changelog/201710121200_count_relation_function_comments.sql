--liquibase formatted sql
--changeset skynet:count_relation_size_function endDelimiter:\n; dbms:oracle stripComments:false
--rollback drop function count_relation_size;
CREATE OR REPLACE FUNCTION count_relation_size(
    p_rel_table_name IN VARCHAR2,
    p_aso_table_name IN VARCHAR2,
    p_id_val_rel_table   IN NUMBER
)
    RETURN NUMBER IS
    --primary key column name of related table
    l_pk_col_name_rel_tab VARCHAR2(30 CHAR);
    --primary key constraint name of related table
    l_pk_con_name_rel_tab VARCHAR2(30 CHAR);
    --column name of primary key of  related table in  associative table
    l_pk_col_name_aso_tab VARCHAR2(30 CHAR);
    --
    l_result              NUMBER;
    BEGIN
        --
        SELECT
            coco.column_name,
            cons.constraint_name
        INTO
            l_pk_col_name_rel_tab, l_pk_con_name_rel_tab
        FROM
            all_constraints cons
            INNER JOIN all_cons_columns coco ON (cons.constraint_name = coco.constraint_name AND cons.owner = coco.owner )
        WHERE
            coco.table_name = p_rel_table_name AND cons.constraint_type = 'P';
        --
        SELECT coco.column_name
        INTO
            l_pk_col_name_aso_tab
        FROM
            all_constraints cons
            INNER JOIN all_cons_columns coco ON (cons.constraint_name = coco.constraint_name AND cons.owner = coco.owner )
        WHERE
            coco.table_name = p_aso_table_name AND cons.r_constraint_name = l_pk_con_name_rel_tab AND cons.constraint_type = 'R';
        EXECUTE IMMEDIATE 'SELECT count(1)  FROM '|| p_aso_table_name || ' WHERE ' || l_pk_col_name_aso_tab || ' = ' || p_id_val_rel_table
            INTO l_result;
        --
        --
        RETURN l_result;
        --
        EXCEPTION
        WHEN no_data_found OR too_many_rows THEN
            raise_application_error(-20001,'sql code: ' || sqlcode|| 'sqlerm: '|| sqlerrm || ' - probablu wrong tabale name');
    END;
