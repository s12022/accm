--liquibase formatted sql
--changeset skynet:chk_constraints_consumer3 dbms:oracle
ALTER TABLE CONSUMER ADD CONSTRAINT CONSUMER_EMAIL_CHK CHECK (REGEXP_LIKE(EMAIL, '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$'));
