--liquibase formatted sql
--changeset skynet:consumer_interests_validation9_consumer_branches_air splitStatements:false dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_BRANCHES_AIR
AFTER INSERT
    ON CONSUMER_BRANCHES
FOR EACH ROW
    DECLARE
        l_branches_count CONSUMER.BRANCHES_COUNT%TYPE;
    BEGIN
        SELECT BRANCHES_COUNT
        INTO l_branches_count
        FROM CONSUMER
        WHERE ID = :NEW.CONSUMERS_ID;
        --
        l_branches_count := l_branches_count + 1;
        --
        UPDATE CONSUMER
        SET BRANCHES_COUNT = l_branches_count
        WHERE ID = :NEW.CONSUMERS_ID;
    END;
