--liquibase formatted sql
--changeset skynet:consumer_interests_validation4_5_6_consumer_therapies_drop_unnessesary_triggers_air endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_THERAPIES_AIR
--changeset skynet:consumer_interests_validation4_5_6_consumer_therapies_drop_unnessesary_triggers_aur endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_THERAPIES_AUR
--changeset skynet:consumer_interests_validation4_5_6_consumer_therapies_drop_unnessesary_triggers_adr endDelimiter:\n; dbms:oracle
DROP TRIGGER CONSUMER_THERAPIES_ADR
--changeset skynet:consumer_interests_validation4_5_6_consumer_therapies_aiudr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_THERAPIES_AIDUR
AFTER DELETE OR UPDATE OR INSERT
    ON CONSUMER_THERAPIES
FOR EACH ROW
    --
    DECLARE
        l_therapies_count CONSUMER.THERAPIES_COUNT%TYPE;
    BEGIN
        IF INSERTING
        THEN
            --
            BEGIN
                SELECT THERAPIES_COUNT
                INTO l_therapies_count
                FROM CONSUMER
                WHERE ID = :NEW.CONSUMERS_ID;
                --
                l_therapies_count := l_therapies_count + 1;
                --
                UPDATE CONSUMER
                SET THERAPIES_COUNT = l_therapies_count
                WHERE ID = :NEW.CONSUMERS_ID;
            END;
            --
            --
        ELSIF DELETING
            THEN
                --
                BEGIN
                    SELECT THERAPIES_COUNT
                    INTO l_therapies_count
                    FROM CONSUMER
                    WHERE ID = :OLD.CONSUMERS_ID;
                    --
                    IF l_therapies_count > 0
                    THEN
                        l_therapies_count := l_therapies_count - 1;
                        UPDATE CONSUMER
                        SET THERAPIES_COUNT = l_therapies_count
                        WHERE ID = :OLD.CONSUMERS_ID;
                    END IF;
                END;
                --
                --
        ELSIF UPDATING
            THEN
                --
                DECLARE
                    l_therapies_count_old CONSUMER.THERAPIES_COUNT%TYPE;
                    l_therapies_count_new CONSUMER.THERAPIES_COUNT%TYPE;
                BEGIN
                    --
                    IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
                    THEN
                        --
                        SELECT THERAPIES_COUNT
                        INTO l_therapies_count_old
                        FROM CONSUMER
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        IF l_therapies_count_old > 0
                        THEN
                            l_therapies_count_old := l_therapies_count_old - 1;
                        END IF;
                        --
                        --
                        UPDATE CONSUMER
                        SET THERAPIES_COUNT = l_therapies_count_old
                        WHERE ID = :OLD.CONSUMERS_ID;
                        --
                        --
                        SELECT THERAPIES_COUNT
                        INTO l_therapies_count_new
                        FROM CONSUMER
                        WHERE ID = :NEW.CONSUMERS_ID;
                        l_therapies_count_new := l_therapies_count_new + 1;
                        --
                        UPDATE CONSUMER
                        SET THERAPIES_COUNT = l_therapies_count_new
                        WHERE ID = :NEW.CONSUMERS_ID;
                    END IF;
                    --
                END;
                --
        END IF;
    END;
