--liquibase formatted sql
--changeset skynet:chk_constraints_address dbms:oracle
CREATE UNIQUE INDEX ADDRESS_PRNA_CINA_STNA_UK ON ADDRESS (PROVINCE_NAME, CITY_NAME, STREET_NAME);
