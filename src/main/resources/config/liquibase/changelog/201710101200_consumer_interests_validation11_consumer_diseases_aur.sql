--liquibase formatted sql
--changeset skynet:consumer_interests_validation11_consumer_diseases_aur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DISEASES_AUR
AFTER UPDATE
    ON CONSUMER_DISEASES
FOR EACH ROW
    DECLARE
        l_diseases_count_old CONSUMER.DISEASES_COUNT%TYPE;
        l_diseases_count_new CONSUMER.DISEASES_COUNT%TYPE;
    BEGIN
        --
        IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
        THEN
            --
            SELECT DISEASES_COUNT
            INTO l_diseases_count_old
            FROM CONSUMER
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            IF l_diseases_count_old > 0
            THEN
                l_diseases_count_old := l_diseases_count_old - 1;
            END IF;
            --
            --
            UPDATE CONSUMER
            SET DISEASES_COUNT = l_diseases_count_old
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            --
            SELECT DISEASES_COUNT
            INTO l_diseases_count_new
            FROM CONSUMER
            WHERE ID = :NEW.CONSUMERS_ID;
            l_diseases_count_new := l_diseases_count_new + 1;
            --
            UPDATE CONSUMER
            SET DISEASES_COUNT = l_diseases_count_new
            WHERE ID = :NEW.CONSUMERS_ID;
        END IF;
    END;
