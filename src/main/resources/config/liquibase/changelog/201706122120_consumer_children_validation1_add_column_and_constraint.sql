--liquibase formatted sql
--changeset skynet:consumer_children_validation1 dbms:oracle
ALTER TABLE CHILD
    ADD PARENT_COUNT NUMBER(1, 0) DEFAULT 0 NOT NULL;
--
ALTER TABLE CHILD
    ADD CONSTRAINT CHILD_PARENT_COUNT_CHK CHECK (PARENT_COUNT BETWEEN  0 AND  2);
