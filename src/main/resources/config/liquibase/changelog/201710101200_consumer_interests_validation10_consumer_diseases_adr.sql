--liquibase formatted sql
--changeset skynet:consumer_interests_validation10_consumer_diseases_adr endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_DISEASES_ADR
AFTER DELETE
    ON CONSUMER_DISEASES
FOR EACH ROW
    DECLARE
        l_diseases_count CONSUMER.DISEASES_COUNT%TYPE;
    BEGIN
        SELECT DISEASES_COUNT
        INTO l_diseases_count
        FROM CONSUMER
        WHERE ID = :OLD.CONSUMERS_ID;
        --
        IF l_diseases_count > 0
        THEN
            l_diseases_count := l_diseases_count - 1;
            UPDATE CONSUMER
            SET DISEASES_COUNT = l_diseases_count
            WHERE ID = :OLD.CONSUMERS_ID;
        END IF;
    END;
