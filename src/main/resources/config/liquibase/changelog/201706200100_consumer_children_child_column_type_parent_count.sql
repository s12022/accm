--liquibase formatted sql
--changeset skynet:consumer_children_child_column_type_parent_count dbms:oracle
ALTER TABLE CHILD
    MODIFY PARENT_COUNT NUMBER(38, 0);
