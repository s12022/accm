--liquibase formatted sql
--changeset skynet:consumer_interests_validation5_consumer_therapies_aur endDelimiter:\n; dbms:oracle
CREATE OR REPLACE TRIGGER CONSUMER_THERAPIES_AUR
AFTER UPDATE
    ON CONSUMER_THERAPIES
FOR EACH ROW
    DECLARE
        l_therapies_count_old CONSUMER.THERAPIES_COUNT%TYPE;
        l_therapies_count_new CONSUMER.THERAPIES_COUNT%TYPE;
    BEGIN
        --
        IF :OLD.CONSUMERS_ID != :NEW.CONSUMERS_ID
        THEN
            --
            SELECT THERAPIES_COUNT
            INTO l_therapies_count_old
            FROM CONSUMER
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            IF l_therapies_count_old > 0
            THEN
                l_therapies_count_old := l_therapies_count_old - 1;
            END IF;
            --
            --
            UPDATE CONSUMER
            SET THERAPIES_COUNT = l_therapies_count_old
            WHERE ID = :OLD.CONSUMERS_ID;
            --
            --
            SELECT THERAPIES_COUNT
            INTO l_therapies_count_new
            FROM CONSUMER
            WHERE ID = :NEW.CONSUMERS_ID;
            l_therapies_count_new := l_therapies_count_new + 1;
            --
            UPDATE CONSUMER
            SET THERAPIES_COUNT = l_therapies_count_new
            WHERE ID = :NEW.CONSUMERS_ID;
        END IF;
    END;
