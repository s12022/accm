/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { AccmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ConsumerDetailComponent } from '../../../../../../main/webapp/app/entities/consumer/consumer-detail.component';
import { ConsumerService } from '../../../../../../main/webapp/app/entities/consumer/consumer.service';
import { Consumer } from '../../../../../../main/webapp/app/entities/consumer/consumer.model';

describe('Component Tests', () => {

    describe('Consumer Management Detail Component', () => {
        let comp: ConsumerDetailComponent;
        let fixture: ComponentFixture<ConsumerDetailComponent>;
        let service: ConsumerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AccmTestModule],
                declarations: [ConsumerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ConsumerService,
                    JhiEventManager
                ]
            }).overrideTemplate(ConsumerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConsumerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConsumerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Consumer(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.consumer).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
