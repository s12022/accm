/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { AccmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DatasourceDetailComponent } from '../../../../../../main/webapp/app/entities/datasource/datasource-detail.component';
import { DatasourceService } from '../../../../../../main/webapp/app/entities/datasource/datasource.service';
import { Datasource } from '../../../../../../main/webapp/app/entities/datasource/datasource.model';

describe('Component Tests', () => {

    describe('Datasource Management Detail Component', () => {
        let comp: DatasourceDetailComponent;
        let fixture: ComponentFixture<DatasourceDetailComponent>;
        let service: DatasourceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AccmTestModule],
                declarations: [DatasourceDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DatasourceService,
                    JhiEventManager
                ]
            }).overrideTemplate(DatasourceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DatasourceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DatasourceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Datasource(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.datasource).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
