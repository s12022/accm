/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { AccmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ActiontypeDetailComponent } from '../../../../../../main/webapp/app/entities/actiontype/actiontype-detail.component';
import { ActiontypeService } from '../../../../../../main/webapp/app/entities/actiontype/actiontype.service';
import { Actiontype } from '../../../../../../main/webapp/app/entities/actiontype/actiontype.model';

describe('Component Tests', () => {

    describe('Actiontype Management Detail Component', () => {
        let comp: ActiontypeDetailComponent;
        let fixture: ComponentFixture<ActiontypeDetailComponent>;
        let service: ActiontypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AccmTestModule],
                declarations: [ActiontypeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ActiontypeService,
                    JhiEventManager
                ]
            }).overrideTemplate(ActiontypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActiontypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActiontypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Actiontype(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.actiontype).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
