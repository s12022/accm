package com.skynet.accm.web.rest;

import com.skynet.accm.AccmApp;

import com.skynet.accm.domain.Therapy;
import com.skynet.accm.domain.Company;
import com.skynet.accm.repository.TherapyRepository;
import com.skynet.accm.repository.search.TherapySearchRepository;
import com.skynet.accm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TherapyResource REST controller.
 *
 * @see TherapyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccmApp.class)
public class TherapyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TherapyRepository therapyRepository;

    @Autowired
    private TherapySearchRepository therapySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTherapyMockMvc;

    private Therapy therapy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TherapyResource therapyResource = new TherapyResource(therapyRepository, therapySearchRepository);
        this.restTherapyMockMvc = MockMvcBuilders.standaloneSetup(therapyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Therapy createEntity(EntityManager em) {
        Therapy therapy = new Therapy()
            .name(DEFAULT_NAME);
        // Add required entity
        Company companies = CompanyResourceIntTest.createEntity(em);
        em.persist(companies);
        em.flush();
        therapy.getCompanies().add(companies);
        return therapy;
    }

    @Before
    public void initTest() {
        therapySearchRepository.deleteAll();
        therapy = createEntity(em);
    }

    @Test
    @Transactional
    public void createTherapy() throws Exception {
        int databaseSizeBeforeCreate = therapyRepository.findAll().size();

        // Create the Therapy
        restTherapyMockMvc.perform(post("/api/therapies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(therapy)))
            .andExpect(status().isCreated());

        // Validate the Therapy in the database
        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeCreate + 1);
        Therapy testTherapy = therapyList.get(therapyList.size() - 1);
        assertThat(testTherapy.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the Therapy in Elasticsearch
        Therapy therapyEs = therapySearchRepository.findOne(testTherapy.getId());
        assertThat(therapyEs).isEqualToComparingFieldByField(testTherapy);
    }

    @Test
    @Transactional
    public void createTherapyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = therapyRepository.findAll().size();

        // Create the Therapy with an existing ID
        therapy.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTherapyMockMvc.perform(post("/api/therapies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(therapy)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = therapyRepository.findAll().size();
        // set the field null
        therapy.setName(null);

        // Create the Therapy, which fails.

        restTherapyMockMvc.perform(post("/api/therapies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(therapy)))
            .andExpect(status().isBadRequest());

        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTherapies() throws Exception {
        // Initialize the database
        therapyRepository.saveAndFlush(therapy);

        // Get all the therapyList
        restTherapyMockMvc.perform(get("/api/therapies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(therapy.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTherapy() throws Exception {
        // Initialize the database
        therapyRepository.saveAndFlush(therapy);

        // Get the therapy
        restTherapyMockMvc.perform(get("/api/therapies/{id}", therapy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(therapy.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTherapy() throws Exception {
        // Get the therapy
        restTherapyMockMvc.perform(get("/api/therapies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTherapy() throws Exception {
        // Initialize the database
        therapyRepository.saveAndFlush(therapy);
        therapySearchRepository.save(therapy);
        int databaseSizeBeforeUpdate = therapyRepository.findAll().size();

        // Update the therapy
        Therapy updatedTherapy = therapyRepository.findOne(therapy.getId());
        updatedTherapy
            .name(UPDATED_NAME);

        restTherapyMockMvc.perform(put("/api/therapies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTherapy)))
            .andExpect(status().isOk());

        // Validate the Therapy in the database
        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeUpdate);
        Therapy testTherapy = therapyList.get(therapyList.size() - 1);
        assertThat(testTherapy.getName()).isEqualTo(UPDATED_NAME);

        // Validate the Therapy in Elasticsearch
        Therapy therapyEs = therapySearchRepository.findOne(testTherapy.getId());
        assertThat(therapyEs).isEqualToComparingFieldByField(testTherapy);
    }

    @Test
    @Transactional
    public void updateNonExistingTherapy() throws Exception {
        int databaseSizeBeforeUpdate = therapyRepository.findAll().size();

        // Create the Therapy

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTherapyMockMvc.perform(put("/api/therapies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(therapy)))
            .andExpect(status().isCreated());

        // Validate the Therapy in the database
        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTherapy() throws Exception {
        // Initialize the database
        therapyRepository.saveAndFlush(therapy);
        therapySearchRepository.save(therapy);
        int databaseSizeBeforeDelete = therapyRepository.findAll().size();

        // Get the therapy
        restTherapyMockMvc.perform(delete("/api/therapies/{id}", therapy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean therapyExistsInEs = therapySearchRepository.exists(therapy.getId());
        assertThat(therapyExistsInEs).isFalse();

        // Validate the database is empty
        List<Therapy> therapyList = therapyRepository.findAll();
        assertThat(therapyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTherapy() throws Exception {
        // Initialize the database
        therapyRepository.saveAndFlush(therapy);
        therapySearchRepository.save(therapy);

        // Search the therapy
        restTherapyMockMvc.perform(get("/api/_search/therapies?query=id:" + therapy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(therapy.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Therapy.class);
        Therapy therapy1 = new Therapy();
        therapy1.setId(1L);
        Therapy therapy2 = new Therapy();
        therapy2.setId(therapy1.getId());
        assertThat(therapy1).isEqualTo(therapy2);
        therapy2.setId(2L);
        assertThat(therapy1).isNotEqualTo(therapy2);
        therapy1.setId(null);
        assertThat(therapy1).isNotEqualTo(therapy2);
    }
}
