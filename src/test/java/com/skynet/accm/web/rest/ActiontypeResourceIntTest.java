package com.skynet.accm.web.rest;

import com.skynet.accm.AccmApp;

import com.skynet.accm.domain.Actiontype;
import com.skynet.accm.repository.ActiontypeRepository;
import com.skynet.accm.repository.search.ActiontypeSearchRepository;
import com.skynet.accm.service.dto.ActiontypeDTO;
import com.skynet.accm.service.mapper.ActiontypeMapper;
import com.skynet.accm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ActiontypeResource REST controller.
 *
 * @see ActiontypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccmApp.class)
public class ActiontypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ActiontypeRepository actiontypeRepository;

    @Autowired
    private ActiontypeMapper actiontypeMapper;

    @Autowired
    private ActiontypeSearchRepository actiontypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restActiontypeMockMvc;

    private Actiontype actiontype;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ActiontypeResource actiontypeResource = new ActiontypeResource(actiontypeRepository, actiontypeMapper, actiontypeSearchRepository);
        this.restActiontypeMockMvc = MockMvcBuilders.standaloneSetup(actiontypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Actiontype createEntity(EntityManager em) {
        Actiontype actiontype = new Actiontype()
            .name(DEFAULT_NAME);
        return actiontype;
    }

    @Before
    public void initTest() {
        actiontypeSearchRepository.deleteAll();
        actiontype = createEntity(em);
    }

    @Test
    @Transactional
    public void createActiontype() throws Exception {
        int databaseSizeBeforeCreate = actiontypeRepository.findAll().size();

        // Create the Actiontype
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(actiontype);
        restActiontypeMockMvc.perform(post("/api/actiontypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(actiontypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Actiontype in the database
        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeCreate + 1);
        Actiontype testActiontype = actiontypeList.get(actiontypeList.size() - 1);
        assertThat(testActiontype.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the Actiontype in Elasticsearch
        Actiontype actiontypeEs = actiontypeSearchRepository.findOne(testActiontype.getId());
        assertThat(actiontypeEs).isEqualToComparingFieldByField(testActiontype);
    }

    @Test
    @Transactional
    public void createActiontypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = actiontypeRepository.findAll().size();

        // Create the Actiontype with an existing ID
        actiontype.setId(1L);
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(actiontype);

        // An entity with an existing ID cannot be created, so this API call must fail
        restActiontypeMockMvc.perform(post("/api/actiontypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(actiontypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = actiontypeRepository.findAll().size();
        // set the field null
        actiontype.setName(null);

        // Create the Actiontype, which fails.
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(actiontype);

        restActiontypeMockMvc.perform(post("/api/actiontypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(actiontypeDTO)))
            .andExpect(status().isBadRequest());

        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllActiontypes() throws Exception {
        // Initialize the database
        actiontypeRepository.saveAndFlush(actiontype);

        // Get all the actiontypeList
        restActiontypeMockMvc.perform(get("/api/actiontypes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(actiontype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getActiontype() throws Exception {
        // Initialize the database
        actiontypeRepository.saveAndFlush(actiontype);

        // Get the actiontype
        restActiontypeMockMvc.perform(get("/api/actiontypes/{id}", actiontype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(actiontype.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingActiontype() throws Exception {
        // Get the actiontype
        restActiontypeMockMvc.perform(get("/api/actiontypes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActiontype() throws Exception {
        // Initialize the database
        actiontypeRepository.saveAndFlush(actiontype);
        actiontypeSearchRepository.save(actiontype);
        int databaseSizeBeforeUpdate = actiontypeRepository.findAll().size();

        // Update the actiontype
        Actiontype updatedActiontype = actiontypeRepository.findOne(actiontype.getId());
        updatedActiontype
            .name(UPDATED_NAME);
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(updatedActiontype);

        restActiontypeMockMvc.perform(put("/api/actiontypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(actiontypeDTO)))
            .andExpect(status().isOk());

        // Validate the Actiontype in the database
        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeUpdate);
        Actiontype testActiontype = actiontypeList.get(actiontypeList.size() - 1);
        assertThat(testActiontype.getName()).isEqualTo(UPDATED_NAME);

        // Validate the Actiontype in Elasticsearch
        Actiontype actiontypeEs = actiontypeSearchRepository.findOne(testActiontype.getId());
        assertThat(actiontypeEs).isEqualToComparingFieldByField(testActiontype);
    }

    @Test
    @Transactional
    public void updateNonExistingActiontype() throws Exception {
        int databaseSizeBeforeUpdate = actiontypeRepository.findAll().size();

        // Create the Actiontype
        ActiontypeDTO actiontypeDTO = actiontypeMapper.toDto(actiontype);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restActiontypeMockMvc.perform(put("/api/actiontypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(actiontypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Actiontype in the database
        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteActiontype() throws Exception {
        // Initialize the database
        actiontypeRepository.saveAndFlush(actiontype);
        actiontypeSearchRepository.save(actiontype);
        int databaseSizeBeforeDelete = actiontypeRepository.findAll().size();

        // Get the actiontype
        restActiontypeMockMvc.perform(delete("/api/actiontypes/{id}", actiontype.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean actiontypeExistsInEs = actiontypeSearchRepository.exists(actiontype.getId());
        assertThat(actiontypeExistsInEs).isFalse();

        // Validate the database is empty
        List<Actiontype> actiontypeList = actiontypeRepository.findAll();
        assertThat(actiontypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchActiontype() throws Exception {
        // Initialize the database
        actiontypeRepository.saveAndFlush(actiontype);
        actiontypeSearchRepository.save(actiontype);

        // Search the actiontype
        restActiontypeMockMvc.perform(get("/api/_search/actiontypes?query=id:" + actiontype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(actiontype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Actiontype.class);
        Actiontype actiontype1 = new Actiontype();
        actiontype1.setId(1L);
        Actiontype actiontype2 = new Actiontype();
        actiontype2.setId(actiontype1.getId());
        assertThat(actiontype1).isEqualTo(actiontype2);
        actiontype2.setId(2L);
        assertThat(actiontype1).isNotEqualTo(actiontype2);
        actiontype1.setId(null);
        assertThat(actiontype1).isNotEqualTo(actiontype2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActiontypeDTO.class);
        ActiontypeDTO actiontypeDTO1 = new ActiontypeDTO();
        actiontypeDTO1.setId(1L);
        ActiontypeDTO actiontypeDTO2 = new ActiontypeDTO();
        assertThat(actiontypeDTO1).isNotEqualTo(actiontypeDTO2);
        actiontypeDTO2.setId(actiontypeDTO1.getId());
        assertThat(actiontypeDTO1).isEqualTo(actiontypeDTO2);
        actiontypeDTO2.setId(2L);
        assertThat(actiontypeDTO1).isNotEqualTo(actiontypeDTO2);
        actiontypeDTO1.setId(null);
        assertThat(actiontypeDTO1).isNotEqualTo(actiontypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(actiontypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(actiontypeMapper.fromId(null)).isNull();
    }
}
