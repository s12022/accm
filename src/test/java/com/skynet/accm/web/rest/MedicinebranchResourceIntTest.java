package com.skynet.accm.web.rest;

import com.skynet.accm.AccmApp;

import com.skynet.accm.domain.Medicinebranch;
import com.skynet.accm.domain.Company;
import com.skynet.accm.repository.MedicinebranchRepository;
import com.skynet.accm.repository.search.MedicinebranchSearchRepository;
import com.skynet.accm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MedicinebranchResource REST controller.
 *
 * @see MedicinebranchResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccmApp.class)
public class MedicinebranchResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MedicinebranchRepository medicinebranchRepository;

    @Autowired
    private MedicinebranchSearchRepository medicinebranchSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMedicinebranchMockMvc;

    private Medicinebranch medicinebranch;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MedicinebranchResource medicinebranchResource = new MedicinebranchResource(medicinebranchRepository, medicinebranchSearchRepository);
        this.restMedicinebranchMockMvc = MockMvcBuilders.standaloneSetup(medicinebranchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medicinebranch createEntity(EntityManager em) {
        Medicinebranch medicinebranch = new Medicinebranch()
            .name(DEFAULT_NAME);
        // Add required entity
        Company company = CompanyResourceIntTest.createEntity(em);
        em.persist(company);
        em.flush();
        medicinebranch.getCompanies().add(company);
        return medicinebranch;
    }

    @Before
    public void initTest() {
        medicinebranchSearchRepository.deleteAll();
        medicinebranch = createEntity(em);
    }

    @Test
    @Transactional
    public void createMedicinebranch() throws Exception {
        int databaseSizeBeforeCreate = medicinebranchRepository.findAll().size();

        // Create the Medicinebranch
        restMedicinebranchMockMvc.perform(post("/api/medicinebranches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medicinebranch)))
            .andExpect(status().isCreated());

        // Validate the Medicinebranch in the database
        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeCreate + 1);
        Medicinebranch testMedicinebranch = medicinebranchList.get(medicinebranchList.size() - 1);
        assertThat(testMedicinebranch.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the Medicinebranch in Elasticsearch
        Medicinebranch medicinebranchEs = medicinebranchSearchRepository.findOne(testMedicinebranch.getId());
        assertThat(medicinebranchEs).isEqualToComparingFieldByField(testMedicinebranch);
    }

    @Test
    @Transactional
    public void createMedicinebranchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = medicinebranchRepository.findAll().size();

        // Create the Medicinebranch with an existing ID
        medicinebranch.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMedicinebranchMockMvc.perform(post("/api/medicinebranches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medicinebranch)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = medicinebranchRepository.findAll().size();
        // set the field null
        medicinebranch.setName(null);

        // Create the Medicinebranch, which fails.

        restMedicinebranchMockMvc.perform(post("/api/medicinebranches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medicinebranch)))
            .andExpect(status().isBadRequest());

        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMedicinebranches() throws Exception {
        // Initialize the database
        medicinebranchRepository.saveAndFlush(medicinebranch);

        // Get all the medicinebranchList
        restMedicinebranchMockMvc.perform(get("/api/medicinebranches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medicinebranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMedicinebranch() throws Exception {
        // Initialize the database
        medicinebranchRepository.saveAndFlush(medicinebranch);

        // Get the medicinebranch
        restMedicinebranchMockMvc.perform(get("/api/medicinebranches/{id}", medicinebranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(medicinebranch.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMedicinebranch() throws Exception {
        // Get the medicinebranch
        restMedicinebranchMockMvc.perform(get("/api/medicinebranches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMedicinebranch() throws Exception {
        // Initialize the database
        medicinebranchRepository.saveAndFlush(medicinebranch);
        medicinebranchSearchRepository.save(medicinebranch);
        int databaseSizeBeforeUpdate = medicinebranchRepository.findAll().size();

        // Update the medicinebranch
        Medicinebranch updatedMedicinebranch = medicinebranchRepository.findOne(medicinebranch.getId());
        updatedMedicinebranch
            .name(UPDATED_NAME);

        restMedicinebranchMockMvc.perform(put("/api/medicinebranches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMedicinebranch)))
            .andExpect(status().isOk());

        // Validate the Medicinebranch in the database
        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeUpdate);
        Medicinebranch testMedicinebranch = medicinebranchList.get(medicinebranchList.size() - 1);
        assertThat(testMedicinebranch.getName()).isEqualTo(UPDATED_NAME);

        // Validate the Medicinebranch in Elasticsearch
        Medicinebranch medicinebranchEs = medicinebranchSearchRepository.findOne(testMedicinebranch.getId());
        assertThat(medicinebranchEs).isEqualToComparingFieldByField(testMedicinebranch);
    }

    @Test
    @Transactional
    public void updateNonExistingMedicinebranch() throws Exception {
        int databaseSizeBeforeUpdate = medicinebranchRepository.findAll().size();

        // Create the Medicinebranch

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMedicinebranchMockMvc.perform(put("/api/medicinebranches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medicinebranch)))
            .andExpect(status().isCreated());

        // Validate the Medicinebranch in the database
        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMedicinebranch() throws Exception {
        // Initialize the database
        medicinebranchRepository.saveAndFlush(medicinebranch);
        medicinebranchSearchRepository.save(medicinebranch);
        int databaseSizeBeforeDelete = medicinebranchRepository.findAll().size();

        // Get the medicinebranch
        restMedicinebranchMockMvc.perform(delete("/api/medicinebranches/{id}", medicinebranch.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean medicinebranchExistsInEs = medicinebranchSearchRepository.exists(medicinebranch.getId());
        assertThat(medicinebranchExistsInEs).isFalse();

        // Validate the database is empty
        List<Medicinebranch> medicinebranchList = medicinebranchRepository.findAll();
        assertThat(medicinebranchList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMedicinebranch() throws Exception {
        // Initialize the database
        medicinebranchRepository.saveAndFlush(medicinebranch);
        medicinebranchSearchRepository.save(medicinebranch);

        // Search the medicinebranch
        restMedicinebranchMockMvc.perform(get("/api/_search/medicinebranches?query=id:" + medicinebranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medicinebranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Medicinebranch.class);
        Medicinebranch medicinebranch1 = new Medicinebranch();
        medicinebranch1.setId(1L);
        Medicinebranch medicinebranch2 = new Medicinebranch();
        medicinebranch2.setId(medicinebranch1.getId());
        assertThat(medicinebranch1).isEqualTo(medicinebranch2);
        medicinebranch2.setId(2L);
        assertThat(medicinebranch1).isNotEqualTo(medicinebranch2);
        medicinebranch1.setId(null);
        assertThat(medicinebranch1).isNotEqualTo(medicinebranch2);
    }
}
