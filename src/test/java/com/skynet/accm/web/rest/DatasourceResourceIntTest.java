package com.skynet.accm.web.rest;

import com.skynet.accm.AccmApp;

import com.skynet.accm.domain.Datasource;
import com.skynet.accm.domain.Actiontype;
import com.skynet.accm.domain.Division;
import com.skynet.accm.repository.DatasourceRepository;
import com.skynet.accm.repository.search.DatasourceSearchRepository;
import com.skynet.accm.service.dto.DatasourceDTO;
import com.skynet.accm.service.mapper.DatasourceMapper;
import com.skynet.accm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.skynet.accm.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DatasourceResource REST controller.
 *
 * @see DatasourceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccmApp.class)
public class DatasourceResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_TURN = 10;
    private static final Integer UPDATED_TURN = 9;

    private static final String DEFAULT_PERSON_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PERSON_NAME = "BBBBBBBBBB";

    @Autowired
    private DatasourceRepository datasourceRepository;

    @Autowired
    private DatasourceMapper datasourceMapper;

    @Autowired
    private DatasourceSearchRepository datasourceSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDatasourceMockMvc;

    private Datasource datasource;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DatasourceResource datasourceResource = new DatasourceResource(datasourceRepository, datasourceMapper, datasourceSearchRepository);
        this.restDatasourceMockMvc = MockMvcBuilders.standaloneSetup(datasourceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Datasource createEntity(EntityManager em) {
        Datasource datasource = new Datasource()
            .title(DEFAULT_TITLE)
            .date(DEFAULT_DATE)
            .turn(DEFAULT_TURN)
            .personName(DEFAULT_PERSON_NAME);
        // Add required entity
        Actiontype actiontype = ActiontypeResourceIntTest.createEntity(em);
        em.persist(actiontype);
        em.flush();
        datasource.setActiontype(actiontype);
        // Add required entity
        Division division = DivisionResourceIntTest.createEntity(em);
        em.persist(division);
        em.flush();
        datasource.setDivision(division);
        return datasource;
    }

    @Before
    public void initTest() {
        datasourceSearchRepository.deleteAll();
        datasource = createEntity(em);
    }

    @Test
    @Transactional
    public void createDatasource() throws Exception {
        int databaseSizeBeforeCreate = datasourceRepository.findAll().size();

        // Create the Datasource
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);
        restDatasourceMockMvc.perform(post("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isCreated());

        // Validate the Datasource in the database
        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeCreate + 1);
        Datasource testDatasource = datasourceList.get(datasourceList.size() - 1);
        assertThat(testDatasource.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDatasource.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testDatasource.getTurn()).isEqualTo(DEFAULT_TURN);
        assertThat(testDatasource.getPersonName()).isEqualTo(DEFAULT_PERSON_NAME);

        // Validate the Datasource in Elasticsearch
        Datasource datasourceEs = datasourceSearchRepository.findOne(testDatasource.getId());
        assertThat(datasourceEs).isEqualToComparingFieldByField(testDatasource);
    }

    @Test
    @Transactional
    public void createDatasourceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = datasourceRepository.findAll().size();

        // Create the Datasource with an existing ID
        datasource.setId(1L);
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDatasourceMockMvc.perform(post("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasourceRepository.findAll().size();
        // set the field null
        datasource.setTitle(null);

        // Create the Datasource, which fails.
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);

        restDatasourceMockMvc.perform(post("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isBadRequest());

        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasourceRepository.findAll().size();
        // set the field null
        datasource.setDate(null);

        // Create the Datasource, which fails.
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);

        restDatasourceMockMvc.perform(post("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isBadRequest());

        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDatasources() throws Exception {
        // Initialize the database
        datasourceRepository.saveAndFlush(datasource);

        // Get all the datasourceList
        restDatasourceMockMvc.perform(get("/api/datasources?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datasource.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].turn").value(hasItem(DEFAULT_TURN)))
            .andExpect(jsonPath("$.[*].personName").value(hasItem(DEFAULT_PERSON_NAME.toString())));
    }

    @Test
    @Transactional
    public void getDatasource() throws Exception {
        // Initialize the database
        datasourceRepository.saveAndFlush(datasource);

        // Get the datasource
        restDatasourceMockMvc.perform(get("/api/datasources/{id}", datasource.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(datasource.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)))
            .andExpect(jsonPath("$.turn").value(DEFAULT_TURN))
            .andExpect(jsonPath("$.personName").value(DEFAULT_PERSON_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDatasource() throws Exception {
        // Get the datasource
        restDatasourceMockMvc.perform(get("/api/datasources/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDatasource() throws Exception {
        // Initialize the database
        datasourceRepository.saveAndFlush(datasource);
        datasourceSearchRepository.save(datasource);
        int databaseSizeBeforeUpdate = datasourceRepository.findAll().size();

        // Update the datasource
        Datasource updatedDatasource = datasourceRepository.findOne(datasource.getId());
        updatedDatasource
            .title(UPDATED_TITLE)
            .date(UPDATED_DATE)
            .turn(UPDATED_TURN)
            .personName(UPDATED_PERSON_NAME);
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(updatedDatasource);

        restDatasourceMockMvc.perform(put("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isOk());

        // Validate the Datasource in the database
        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeUpdate);
        Datasource testDatasource = datasourceList.get(datasourceList.size() - 1);
        assertThat(testDatasource.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDatasource.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testDatasource.getTurn()).isEqualTo(UPDATED_TURN);
        assertThat(testDatasource.getPersonName()).isEqualTo(UPDATED_PERSON_NAME);

        // Validate the Datasource in Elasticsearch
        Datasource datasourceEs = datasourceSearchRepository.findOne(testDatasource.getId());
        assertThat(datasourceEs).isEqualToComparingFieldByField(testDatasource);
    }

    @Test
    @Transactional
    public void updateNonExistingDatasource() throws Exception {
        int databaseSizeBeforeUpdate = datasourceRepository.findAll().size();

        // Create the Datasource
        DatasourceDTO datasourceDTO = datasourceMapper.toDto(datasource);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDatasourceMockMvc.perform(put("/api/datasources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasourceDTO)))
            .andExpect(status().isCreated());

        // Validate the Datasource in the database
        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDatasource() throws Exception {
        // Initialize the database
        datasourceRepository.saveAndFlush(datasource);
        datasourceSearchRepository.save(datasource);
        int databaseSizeBeforeDelete = datasourceRepository.findAll().size();

        // Get the datasource
        restDatasourceMockMvc.perform(delete("/api/datasources/{id}", datasource.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean datasourceExistsInEs = datasourceSearchRepository.exists(datasource.getId());
        assertThat(datasourceExistsInEs).isFalse();

        // Validate the database is empty
        List<Datasource> datasourceList = datasourceRepository.findAll();
        assertThat(datasourceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDatasource() throws Exception {
        // Initialize the database
        datasourceRepository.saveAndFlush(datasource);
        datasourceSearchRepository.save(datasource);

        // Search the datasource
        restDatasourceMockMvc.perform(get("/api/_search/datasources?query=id:" + datasource.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datasource.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].turn").value(hasItem(DEFAULT_TURN)))
            .andExpect(jsonPath("$.[*].personName").value(hasItem(DEFAULT_PERSON_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Datasource.class);
        Datasource datasource1 = new Datasource();
        datasource1.setId(1L);
        Datasource datasource2 = new Datasource();
        datasource2.setId(datasource1.getId());
        assertThat(datasource1).isEqualTo(datasource2);
        datasource2.setId(2L);
        assertThat(datasource1).isNotEqualTo(datasource2);
        datasource1.setId(null);
        assertThat(datasource1).isNotEqualTo(datasource2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DatasourceDTO.class);
        DatasourceDTO datasourceDTO1 = new DatasourceDTO();
        datasourceDTO1.setId(1L);
        DatasourceDTO datasourceDTO2 = new DatasourceDTO();
        assertThat(datasourceDTO1).isNotEqualTo(datasourceDTO2);
        datasourceDTO2.setId(datasourceDTO1.getId());
        assertThat(datasourceDTO1).isEqualTo(datasourceDTO2);
        datasourceDTO2.setId(2L);
        assertThat(datasourceDTO1).isNotEqualTo(datasourceDTO2);
        datasourceDTO1.setId(null);
        assertThat(datasourceDTO1).isNotEqualTo(datasourceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(datasourceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(datasourceMapper.fromId(null)).isNull();
    }
}
